package br.com.crudcourses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudcoursesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudcoursesApplication.class, args);
	}

}
