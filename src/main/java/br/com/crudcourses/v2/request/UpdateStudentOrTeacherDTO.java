package br.com.crudcourses.v2.request;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record UpdateStudentOrTeacherDTO(

  @Schema(example = "Lucas", requiredMode= RequiredMode.REQUIRED)
  @NotBlank(message = "Name cannot be empty.")
  @Size(max = 100, message = "Name must be at most 100 characters.")
  String name,

  @Schema(example = "21", requiredMode= RequiredMode.REQUIRED)
  @NotNull(message = "Age cannot be null.")
  Integer age

) {}

