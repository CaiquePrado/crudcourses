package br.com.crudcourses.v2.request;

import org.hibernate.validator.constraints.br.CPF;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record RequestCreateTeacherDTO(

  @Schema(example = "Marcos", requiredMode = RequiredMode.REQUIRED)
  @NotBlank(message = "Name cannot be empty.")
  @Size(max = 100, message = "Name must be at most 100 characters.")
  String name,

  @Schema(example = "30", requiredMode = RequiredMode.REQUIRED)
  @NotNull(message = "Age cannot be null.")
  @Min(value = 18, message = "Age must be at least 18.")
  Integer age,

  @Schema(example = "22867229006", requiredMode = RequiredMode.REQUIRED)
  @NotBlank(message = "CPF cannot be empty.")
  @CPF(message = "CPF must be exactly 14 characters.")
  String cpf,

  @Schema(example = "22867229006", requiredMode = RequiredMode.REQUIRED)
  @NotBlank(message = "Work card cannot be empty.")
  String workCard,

  @Schema(example = "5000.0", requiredMode = RequiredMode.REQUIRED)
  @NotNull(message = "Salary cannot be null.")
  @DecimalMin(value = "0.0", inclusive = false, message = "Salary must be greater than 0.")
  Double salary
  
) {}
