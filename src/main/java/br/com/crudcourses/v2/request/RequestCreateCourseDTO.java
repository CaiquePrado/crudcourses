package br.com.crudcourses.v2.request;

import java.util.UUID;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record RequestCreateCourseDTO(

  @Schema(example = "Mathematics", requiredMode = RequiredMode.REQUIRED)
  @NotBlank(message = "Course name cannot be empty.")
  @Size(max = 100, message = "Course name must be at most 100 characters.")
  String courseName,

  @Schema(example = "3fa85f64-5717-4562-b3fc-2c963f66afa6", requiredMode = RequiredMode.REQUIRED)
  @NotNull(message = "Teacher ID cannot be null.")
  UUID teacherId

) {}
