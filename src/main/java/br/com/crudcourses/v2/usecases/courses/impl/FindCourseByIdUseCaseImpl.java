package br.com.crudcourses.v2.usecases.courses.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.FindCourseByIdUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindCourseByIdUseCaseImpl implements FindCourseByIdUseCase {
  
  private final CoursesRepository coursesRepository;

  @Override
  public Courses find(UUID id) {
    return coursesRepository.findById(id)
      .orElseThrow(() -> new InvalidRequestException("No course found with ID " + id));
  }
}
