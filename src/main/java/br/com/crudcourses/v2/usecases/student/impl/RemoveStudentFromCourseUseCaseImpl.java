package br.com.crudcourses.v2.usecases.student.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.FindCourseByIdUseCase;
import br.com.crudcourses.v2.usecases.student.FindStudentByIdUseCase;
import br.com.crudcourses.v2.usecases.student.RemoveStudentFromCourseUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RemoveStudentFromCourseUseCaseImpl implements RemoveStudentFromCourseUseCase {

  private final CoursesRepository courseRepository;
  private final FindStudentByIdUseCase findStudentByIdUseCase;
  private final FindCourseByIdUseCase findCourseByIdUseCase;

  public void execute(UUID courseId, UUID studentId) {
    Courses course = findCourseByIdUseCase.find(courseId);
    Student student = findStudentByIdUseCase.find(studentId);
    course.getStudents().remove(student);
    courseRepository.save(course);
  }
}