package br.com.crudcourses.v2.usecases.teacher;

import br.com.crudcourses.v2.models.Teacher;

public interface CreateTeacherUseCase {
  
  Teacher execute(Teacher teacher);

}
