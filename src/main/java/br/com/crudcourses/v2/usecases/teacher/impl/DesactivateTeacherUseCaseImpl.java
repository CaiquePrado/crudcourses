package br.com.crudcourses.v2.usecases.teacher.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.DesactivateTeacherUseCase;
import br.com.crudcourses.v2.usecases.teacher.FindTeacherByCpfUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DesactivateTeacherUseCaseImpl implements DesactivateTeacherUseCase {

  private final TeacherRepository teacherRepository;
  private final FindTeacherByCpfUseCase findTeacherByCpfUseCase;

  @Override
  public Teacher execute(String cpf) {
    Teacher teacher = findTeacherByCpfUseCase.find(cpf);
    if (Boolean.FALSE.equals(teacher.getActive())) {
      throw new InvalidRequestException("The teacher is already desactivate.");
    }
    teacher.setActive(false);
    teacherRepository.save(teacher);
    return teacher;
  } 
}


