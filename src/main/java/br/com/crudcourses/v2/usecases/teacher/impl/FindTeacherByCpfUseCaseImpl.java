package br.com.crudcourses.v2.usecases.teacher.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.FindTeacherByCpfUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindTeacherByCpfUseCaseImpl implements FindTeacherByCpfUseCase {

  private final TeacherRepository teacherRepository;

  @Override
  public Teacher find(final String cpf) {
    return teacherRepository.findByCpf(cpf)
      .orElseThrow(() -> new IllegalArgumentException("No teacher found with the CPF " + cpf));
  }
}

