package br.com.crudcourses.v2.usecases.courses;

import org.springframework.data.domain.Page;

import br.com.crudcourses.v2.models.Courses;

public interface ListAllCoursesUseCase {
  
   Page<Courses> execute(int page, int size);

}
