package br.com.crudcourses.v2.usecases.courses;

public interface DeleteCoursesUseCase {
  
  void execute(final String courseName);
  
}
