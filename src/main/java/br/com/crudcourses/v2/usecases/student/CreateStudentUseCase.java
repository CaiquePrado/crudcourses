package br.com.crudcourses.v2.usecases.student;

import br.com.crudcourses.v2.models.Student;

public interface CreateStudentUseCase {
  
  Student execute(Student student);

}
