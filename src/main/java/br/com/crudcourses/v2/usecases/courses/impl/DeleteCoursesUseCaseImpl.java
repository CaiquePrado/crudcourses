package br.com.crudcourses.v2.usecases.courses.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.DeleteCoursesUseCase;
import br.com.crudcourses.v2.usecases.courses.FindCourseByCourseNameUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DeleteCoursesUseCaseImpl implements DeleteCoursesUseCase {

  private final CoursesRepository coursesRepository;
  private final FindCourseByCourseNameUseCase findCourseByNameUseCase;

  @Override
  public void execute(String courseName) {
    Courses course = findCourseByNameUseCase.find(courseName);
    coursesRepository.delete(course);
  }
}
