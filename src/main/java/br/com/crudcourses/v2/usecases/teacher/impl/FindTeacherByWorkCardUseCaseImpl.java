package br.com.crudcourses.v2.usecases.teacher.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.FindTeacherByWorkCardUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindTeacherByWorkCardUseCaseImpl implements FindTeacherByWorkCardUseCase{

  private final TeacherRepository teacherRepository;

  @Override
  public Teacher find(String workCard) {
    return teacherRepository.findByWorkCard(workCard)
      .orElseThrow(() -> new IllegalArgumentException("No teacher found with the work card " + workCard));
  }
  
}
