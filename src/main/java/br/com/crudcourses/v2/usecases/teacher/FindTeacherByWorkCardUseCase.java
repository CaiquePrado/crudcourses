package br.com.crudcourses.v2.usecases.teacher;
import br.com.crudcourses.v2.models.Teacher;

public interface FindTeacherByWorkCardUseCase {
  
  Teacher find(final String workCard);

}
