package br.com.crudcourses.v2.usecases.teacher;

import br.com.crudcourses.v2.models.Teacher;

public interface FindTeacherByCpfUseCase {
  
  Teacher find(final String cpf);

}
