package br.com.crudcourses.v2.usecases.courses.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.CreateCourseUseCase;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class CreateCourseUseCaseImpl implements CreateCourseUseCase {

  private final CoursesRepository coursesRepository;

  @Override
  public Courses execute(Courses course) {
    checkIfCpfExists(course.getCourseName());
    return coursesRepository.save(course);
  }
  
  protected void checkIfCpfExists(String courseName) {
    coursesRepository.findCourseByCourseName(courseName).ifPresent(teacher -> {
      throw new InvalidRequestException("A course with the name " + courseName + " already exists.");
    });
  }
}
