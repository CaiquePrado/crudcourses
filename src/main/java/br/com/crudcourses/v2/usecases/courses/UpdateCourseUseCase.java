package br.com.crudcourses.v2.usecases.courses;

import br.com.crudcourses.v2.models.Courses;

public interface UpdateCourseUseCase {
  
  Courses execute(Courses course, final String courseName);

}
