package br.com.crudcourses.v2.usecases.student;

import java.util.UUID;

public interface CheckStudentInCourseUseCase {
  
  boolean execute(final UUID courseId, final UUID studentId);

}
