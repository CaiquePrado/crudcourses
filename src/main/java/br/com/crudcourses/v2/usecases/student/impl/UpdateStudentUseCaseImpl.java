package br.com.crudcourses.v2.usecases.student.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.FindStudentByCpfUseCase;
import br.com.crudcourses.v2.usecases.student.UpdateStudentUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UpdateStudentUseCaseImpl implements UpdateStudentUseCase {

  private final StudentRepository studentRepository;
  private final FindStudentByCpfUseCase findStudentByCpfUseCase;

  @Override
  public Student execute(Student student, String cpf) {
    Student existingStudent = findStudentByCpfUseCase.find(cpf);
    existingStudent.setName(student.getName());
    existingStudent.setAge(student.getAge());
    return studentRepository.save(existingStudent);
  }
}

