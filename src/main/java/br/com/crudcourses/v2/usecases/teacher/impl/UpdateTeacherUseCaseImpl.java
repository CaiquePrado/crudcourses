package br.com.crudcourses.v2.usecases.teacher.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.FindTeacherByCpfUseCase;
import br.com.crudcourses.v2.usecases.teacher.UpdateTeacherUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UpdateTeacherUseCaseImpl implements UpdateTeacherUseCase {

  private final TeacherRepository teacherRepository;
  private final FindTeacherByCpfUseCase findTeacherByCpfUseCase;

  @Override
  public Teacher execute(Teacher teacher, String cpf) {
    Teacher existingTeacher = findTeacherByCpfUseCase.find(cpf);
    existingTeacher.setName(teacher.getName());
    existingTeacher.setAge(teacher.getAge());
    return teacherRepository.save(existingTeacher);
  }
}

