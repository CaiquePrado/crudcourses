package br.com.crudcourses.v2.usecases.student.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.usecases.courses.FindCourseByIdUseCase;
import br.com.crudcourses.v2.usecases.student.CheckStudentInCourseUseCase;
import br.com.crudcourses.v2.usecases.student.FindStudentByIdUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CheckStudentInCourseUseCaseImpl implements CheckStudentInCourseUseCase {

  private final FindCourseByIdUseCase findCourseByIdUseCase;
  private final FindStudentByIdUseCase findStudentByIdUseCase;

  public boolean execute(UUID courseId, UUID studentId) {
    
    Courses course = findCourseByIdUseCase.find(courseId);

    findStudentByIdUseCase.find(studentId);

    return course.getStudents().stream()
      .anyMatch(student -> student.getId().equals(studentId));
  }
}


