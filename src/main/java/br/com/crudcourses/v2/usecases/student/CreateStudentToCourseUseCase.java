package br.com.crudcourses.v2.usecases.student;

import java.util.UUID;

import br.com.crudcourses.v2.models.Student;

public interface CreateStudentToCourseUseCase {
  
  Student execute(final UUID courseId, final UUID studentId);

}
