package br.com.crudcourses.v2.usecases.courses;

import br.com.crudcourses.v2.models.Courses;

public interface CreateCourseUseCase {

  Courses execute(Courses course);

}