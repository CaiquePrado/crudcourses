package br.com.crudcourses.v2.usecases.student.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.ActivateStudentUseCase;
import br.com.crudcourses.v2.usecases.student.FindStudentByCpfUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ActivateStudentUseCaseImpl implements ActivateStudentUseCase {

  private final StudentRepository studentRepository;
  private final FindStudentByCpfUseCase findStudentByCpfUseCase;

  @Override
  public Student execute(String cpf) {
    Student student = findStudentByCpfUseCase.find(cpf);
    if (Boolean.TRUE.equals(student.getActive())) {
      throw new InvalidRequestException("The Student is already active");
    }
    student.setActive(true);
    studentRepository.save(student);
    return student;
  } 
}

