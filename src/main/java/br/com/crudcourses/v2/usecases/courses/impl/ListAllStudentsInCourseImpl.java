package br.com.crudcourses.v2.usecases.courses.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.FindCourseByCourseNameUseCase;
import br.com.crudcourses.v2.usecases.courses.ListAllStudentsInCourse;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ListAllStudentsInCourseImpl implements ListAllStudentsInCourse {

  private final CoursesRepository coursesRepository;
  private final FindCourseByCourseNameUseCase findCourseByNameUseCase;

  @Override
  public Page<Student> execute(String courseName, int page, int size) {
    findCourseByNameUseCase.find(courseName);
    Pageable pageable = PageRequest.of(page, size);
    return coursesRepository.findActiveStudentsByCourseName(courseName, pageable);
  }
}
