package br.com.crudcourses.v2.usecases.student;

import br.com.crudcourses.v2.models.Student;

public interface FindStudentByCpfUseCase {
  
  Student find(final String cpf);

}
