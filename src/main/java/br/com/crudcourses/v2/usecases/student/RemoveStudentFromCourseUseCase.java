package br.com.crudcourses.v2.usecases.student;

import java.util.UUID;

public interface RemoveStudentFromCourseUseCase{

  void execute(final UUID courseId, final UUID studentI);

}