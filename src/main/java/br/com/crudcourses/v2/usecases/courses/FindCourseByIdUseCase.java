package br.com.crudcourses.v2.usecases.courses;

import java.util.UUID;

import br.com.crudcourses.v2.models.Courses;

public interface FindCourseByIdUseCase {
  
  Courses find(final UUID id);

}
