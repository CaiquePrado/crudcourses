package br.com.crudcourses.v2.usecases.student.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.FindCourseByIdUseCase;
import br.com.crudcourses.v2.usecases.student.CreateStudentToCourseUseCase;
import br.com.crudcourses.v2.usecases.student.FindStudentByIdUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CreateStudentToCourseImpl implements CreateStudentToCourseUseCase {

  private final CoursesRepository courseRepository;
  private final FindStudentByIdUseCase findStudentByIdUseCase;
  private final FindCourseByIdUseCase findCourseByIdUseCase;

  public Student execute(UUID courseId, UUID studentId) {
    Courses course = findCourseByIdUseCase.find(courseId);
    Student student = validateAndFindActiveStudent(studentId);
    course.getStudents().add(student);
    courseRepository.save(course);
    return student;
  }

  protected Student validateAndFindActiveStudent(UUID studentId) {
    Student student = findStudentByIdUseCase.find(studentId);

    if (Boolean.FALSE.equals(student.getActive())) {
      throw new IllegalArgumentException("Student is not active");
    }

    return student;
  }
}

