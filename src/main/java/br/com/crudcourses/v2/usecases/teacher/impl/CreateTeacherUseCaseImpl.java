package br.com.crudcourses.v2.usecases.teacher.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.CreateTeacherUseCase;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class CreateTeacherUseCaseImpl implements CreateTeacherUseCase {

  private final TeacherRepository teacherRepository;
  
  @Override
  public Teacher execute(Teacher teacher) {
    checkIfCpfExists(teacher.getCpf());
    checkIfWorkCardExists(teacher.getWorkCard());
    return teacherRepository.save(teacher);
  }

  protected void checkIfCpfExists(String cpf) {
    teacherRepository.findByCpf(cpf).ifPresent(teacher -> {
      throw new InvalidRequestException("A teacher with the CPF " + cpf + " already exists.");
    });
  }

  protected void checkIfWorkCardExists(String workCard) {
    teacherRepository.findByWorkCard(workCard).ifPresent(teacher -> {
      throw new InvalidRequestException("This work card already exists " + workCard);
    });
  }
}
