package br.com.crudcourses.v2.usecases.courses.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.ListAllCoursesUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ListAllCoursesUseCaseImpl implements ListAllCoursesUseCase {

  private final CoursesRepository coursesRepository;

  @Override
  public Page<Courses> execute(int page, int size) {
    Pageable pageable = PageRequest.of(page, size);
    return coursesRepository.findByPage(pageable);
  }
}
