package br.com.crudcourses.v2.usecases.student.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.DesactivateStudentUseCase;
import br.com.crudcourses.v2.usecases.student.FindStudentByCpfUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DesactivateStudentUseCaseImpl implements DesactivateStudentUseCase {

  private final StudentRepository studentRepository;
  private final FindStudentByCpfUseCase findStudentByCpfUseCase;

  @Override
  public Student execute(String cpf) {
    Student student = findStudentByCpfUseCase.find(cpf);
    if (Boolean.FALSE.equals(student.getActive())) {
      throw new InvalidRequestException("The student is already desactivate");
    }
    student.setActive(false);
    studentRepository.save(student);
    return student;
  } 
}

