package br.com.crudcourses.v2.usecases.student.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.ResourceNotFoundException;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.FindStudentByIdUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindStudentByIdUseCaseImpl implements FindStudentByIdUseCase {
  
  private final StudentRepository studentRepository;

  @Override
  public Student find(UUID id) {
    return studentRepository.findById(id)
      .orElseThrow(() -> new ResourceNotFoundException("No student found with ID " + id));
  }
}

