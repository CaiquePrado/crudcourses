package br.com.crudcourses.v2.usecases.courses;

import org.springframework.data.domain.Page;

import br.com.crudcourses.v2.models.Student;

public interface ListAllStudentsInCourse {
  
  Page<Student> execute(final String courseName, final int page, final int size);

}
