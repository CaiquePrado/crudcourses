package br.com.crudcourses.v2.usecases.teacher.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.ActivateTeacherUseCase;
import br.com.crudcourses.v2.usecases.teacher.FindTeacherByCpfUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ActivateTeacherUseCaseImpl  implements ActivateTeacherUseCase  {

  private final TeacherRepository teacherRepository;
  private final FindTeacherByCpfUseCase findTeacherByCpfUseCase;

  @Override
  public Teacher execute(String cpf) {
    Teacher teacher = findTeacherByCpfUseCase.find(cpf);
    if (Boolean.TRUE.equals(teacher.getActive())) {
      throw new InvalidRequestException("The teacher is already active.");
    }
    teacher.setActive(true);
    teacherRepository.save(teacher);
    return teacher;
  } 
}
