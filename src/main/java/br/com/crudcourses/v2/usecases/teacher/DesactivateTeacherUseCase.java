package br.com.crudcourses.v2.usecases.teacher;

import br.com.crudcourses.v2.models.Teacher;

public interface DesactivateTeacherUseCase {
  
  Teacher execute(final String cpf);
  
}
