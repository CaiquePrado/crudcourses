package br.com.crudcourses.v2.usecases.teacher;

import java.util.UUID;

import br.com.crudcourses.v2.models.Teacher;

public interface FindTeacherByIdUseCase {
  
  Teacher find(final UUID id);

}
