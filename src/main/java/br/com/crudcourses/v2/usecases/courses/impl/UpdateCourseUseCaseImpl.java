package br.com.crudcourses.v2.usecases.courses.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.FindCourseByCourseNameUseCase;
import br.com.crudcourses.v2.usecases.courses.UpdateCourseUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UpdateCourseUseCaseImpl implements UpdateCourseUseCase {

  private final CoursesRepository coursesRepository;
  private final FindCourseByCourseNameUseCase findCourseByNameUseCase;

  @Override
  public Courses execute(Courses course, String courseName) {
    Courses existingCourse = findCourseByNameUseCase.find(courseName);
    existingCourse.setCourseName(course.getCourseName());
    existingCourse.setTeacher(course.getTeacher());
    return coursesRepository.save(existingCourse);
  }
}
