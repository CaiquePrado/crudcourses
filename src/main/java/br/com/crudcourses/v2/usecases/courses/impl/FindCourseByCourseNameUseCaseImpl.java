package br.com.crudcourses.v2.usecases.courses.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.FindCourseByCourseNameUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindCourseByCourseNameUseCaseImpl implements FindCourseByCourseNameUseCase {

  private final CoursesRepository coursesRepository;

  @Override
  public Courses find(String courseName) {
    return coursesRepository.findCourseByCourseName(courseName)
    .orElseThrow(() -> new InvalidRequestException("No course found with name " + courseName));
  }
}
