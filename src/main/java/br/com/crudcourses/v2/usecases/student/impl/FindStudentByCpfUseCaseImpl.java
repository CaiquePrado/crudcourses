package br.com.crudcourses.v2.usecases.student.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.ResourceNotFoundException;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.FindStudentByCpfUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindStudentByCpfUseCaseImpl implements FindStudentByCpfUseCase {

  private final StudentRepository studentRepository;

  @Override
  public Student find(final String cpf) {
    return studentRepository.findByCpf(cpf)
      .orElseThrow(() -> new ResourceNotFoundException("No student found with the CPF " + cpf));
  }
}

