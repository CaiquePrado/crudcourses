package br.com.crudcourses.v2.usecases.teacher;

import br.com.crudcourses.v2.models.Teacher;

public interface UpdateTeacherUseCase {
  
  Teacher execute(Teacher teacher, final String cpf);

}
