package br.com.crudcourses.v2.usecases.student.impl;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.CreateStudentUseCase;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class CreateStudentUseCaseImpl implements CreateStudentUseCase {

  private final StudentRepository studentRepository;

  @Override
  public Student execute(Student student) {
    checkIfCpfExists(student.getCpf());
    return studentRepository.save(student);
  }

  protected void checkIfCpfExists(String cpf) {
    studentRepository.findByCpf(cpf).ifPresent(student -> {
      throw new InvalidRequestException("A student with the CPF " + cpf + " already exists.");
    });
  }
}
