package br.com.crudcourses.v2.usecases.teacher.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.FindTeacherByIdUseCase;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class FindTeacherByIdUseCaseImpl implements FindTeacherByIdUseCase {
  
  private final TeacherRepository teacherRepository;

  @Override
  public Teacher find(UUID id) {
    return teacherRepository.findById(id)
      .orElseThrow(() -> new IllegalArgumentException("No teacher found with ID " + id));
  }
}

