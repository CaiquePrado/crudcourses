package br.com.crudcourses.v2.infra.exceptions;

import java.time.LocalDateTime;

public record ExceptionResponse(
	String message,
	LocalDateTime timestamp
) {}
