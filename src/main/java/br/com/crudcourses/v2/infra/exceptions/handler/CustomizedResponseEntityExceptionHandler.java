package br.com.crudcourses.v2.infra.exceptions.handler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import br.com.crudcourses.v2.infra.exceptions.ExceptionResponse;
import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.infra.exceptions.ResourceNotFoundException;

@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public final ResponseEntity<ExceptionResponse> handleUnexpectedErrors(RuntimeException unexpectedError, WebRequest request) {
        return errorEntity("Unexpected error: " + unexpectedError.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public final ResponseEntity<ExceptionResponse> handleConstraintViolation(MethodArgumentNotValidException e) {
        List<String> errorDetails = e.getBindingResult().getAllErrors().stream()
        .map(ObjectError::getDefaultMessage)
        .collect(Collectors.toList());
        return errorEntity("Please fill in the data correctly " + errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public final ResponseEntity<ExceptionResponse> handleNotFoundExceptions(ResourceNotFoundException ex) {
        return errorEntity(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidRequestException.class)
    public final ResponseEntity<ExceptionResponse> handleInvalidRequestException(InvalidRequestException ex) {
        return errorEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<ExceptionResponse> errorEntity(String message, HttpStatus statusCode) {
        ExceptionResponse errorBody = new ExceptionResponse(message, LocalDateTime.now());
        return new ResponseEntity<>(errorBody, statusCode);
    }
}
