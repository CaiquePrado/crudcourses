package br.com.crudcourses.v2.response;

public record ListAllStudentsInCourseResponseDTO(
  String name
) {}
