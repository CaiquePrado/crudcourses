package br.com.crudcourses.v2.response;

public record UpdateStudentOrTeacherResponseDTO(
  String name,
  Integer age
) {}
