package br.com.crudcourses.v2.response;

import java.util.UUID;

public record CreateOrUpdateCourseResponseDTO(
  UUID id, 
  String courseName, 
  String teacherName, 
  String teacherCPF) {}
