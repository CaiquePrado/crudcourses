package br.com.crudcourses.v2.response;

import java.util.UUID;

public record ListAllCoursesResponseDTO(
  UUID id, 
  String courseName) {}
