package br.com.crudcourses.v2.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.request.RequestCreateTeacherDTO;
import br.com.crudcourses.v2.request.UpdateStudentOrTeacherDTO;

@Mapper(componentModel = "spring")
public interface TeacherMapper {

  @Mapping(target = "id", ignore = true)
  Teacher toTeacher(RequestCreateTeacherDTO createTeacherDTO);

  Teacher toTeacher(UpdateStudentOrTeacherDTO updateStudentDTO);
}
