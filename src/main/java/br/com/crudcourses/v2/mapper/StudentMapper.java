package br.com.crudcourses.v2.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.request.RequestCreateStudentDTO;
import br.com.crudcourses.v2.request.UpdateStudentOrTeacherDTO;

@Mapper(componentModel = "spring")
public interface StudentMapper {

  @Mapping(target = "id", ignore = true)
  Student toStudent(RequestCreateStudentDTO createStudentDTO);

  Student toStudent(UpdateStudentOrTeacherDTO updateStudentDTO);
  
}
