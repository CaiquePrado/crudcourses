package br.com.crudcourses.v2.mapper;

import java.util.UUID;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import br.com.crudcourses.v2.infra.exceptions.ResourceNotFoundException;
import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.request.RequestCreateCourseDTO;
import br.com.crudcourses.v2.request.UpdateCourseDTO;
import br.com.crudcourses.v2.response.CreateOrUpdateCourseResponseDTO;
import br.com.crudcourses.v2.response.ListAllCoursesResponseDTO;
import br.com.crudcourses.v2.response.ListAllStudentsInCourseResponseDTO;

@Mapper(componentModel = "spring", uses = {TeacherRepository.class})
public abstract class CourseMapper {
  
  @Autowired
  private TeacherRepository teacherRepository;

  @Mapping(target = "teacher", expression = "java(getTeacher(createCourseDTO.teacherId()))")
  public abstract Courses toCourse(RequestCreateCourseDTO createCourseDTO);

  protected Teacher getTeacher(UUID teacherId) {
    return teacherRepository.findById(teacherId)
      .orElseThrow(() -> new ResourceNotFoundException("Teacher with id not found"));
  }

  @Mapping(target = "teacher", expression = "java(getTeacher(updateCourseDTO.teacherId()))")
  public abstract Courses toCourse(UpdateCourseDTO updateCourseDTO);

  @Mapping(target = "teacherName", source = "teacher.name")
  @Mapping(target = "teacherCPF", source = "teacher.cpf")
  public abstract CreateOrUpdateCourseResponseDTO toResponse(Courses course);

  public abstract ListAllCoursesResponseDTO toListAllCoursesResponseDTO(Courses course);

  public Page<ListAllCoursesResponseDTO> toCoursePage(Page<Courses> courses){
    return courses.map(this::toListAllCoursesResponseDTO);
  }

  public abstract ListAllStudentsInCourseResponseDTO toStudentDTO(Student student);

  public Page<ListAllStudentsInCourseResponseDTO> toStudentPage(Page<Student> students){
    return students.map(this::toStudentDTO);
  }
}

