package br.com.crudcourses.v2.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.models.Student;

@Repository
public interface CoursesRepository extends JpaRepository<Courses, UUID> {

  Optional<Courses> findCourseByCourseName(String courseName);

  @Query("SELECT c FROM Courses c")
  Page<Courses> findByPage(Pageable pageable);

  @Query("SELECT s FROM Student s JOIN s.courses c WHERE c.courseName = :courseName AND s.active = true")
  Page<Student> findActiveStudentsByCourseName(String courseName, Pageable pageable);
  
}
