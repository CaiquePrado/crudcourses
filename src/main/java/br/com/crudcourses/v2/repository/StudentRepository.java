package br.com.crudcourses.v2.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.crudcourses.v2.models.Student;


@Repository
public interface StudentRepository extends JpaRepository<Student,UUID> {
  
  Optional<Student> findByCpf(String cpf);

}

