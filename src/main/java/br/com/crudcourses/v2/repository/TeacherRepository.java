package br.com.crudcourses.v2.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.crudcourses.v2.models.Teacher;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher,UUID> {

  Optional<Teacher> findByCpf(String cpf);

  Optional<Teacher> findByWorkCard(String workCard);
  
}
