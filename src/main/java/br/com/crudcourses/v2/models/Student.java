package br.com.crudcourses.v2.models;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "tb_student")
@SuperBuilder
@NoArgsConstructor
public class Student extends Person {
  
  @ManyToMany(mappedBy = "students")
  private List<Courses> courses;
  
}
