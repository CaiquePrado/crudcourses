package br.com.crudcourses.v2.models;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "tb_teacher")
@NoArgsConstructor
@SuperBuilder
public class Teacher extends Person {
  
  @Getter
  @Setter
  private String workCard;

  @Getter
  @Setter
  private Double salary;

  @OneToMany(mappedBy="teacher")
  private List<Courses> courses;

}
