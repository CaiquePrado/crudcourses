package br.com.crudcourses.v2.controllers.impl;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.crudcourses.v2.controllers.CoursesController;
import br.com.crudcourses.v2.mapper.CourseMapper;
import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.request.RequestCreateCourseDTO;
import br.com.crudcourses.v2.request.UpdateCourseDTO;
import br.com.crudcourses.v2.response.CreateOrUpdateCourseResponseDTO;
import br.com.crudcourses.v2.response.ListAllCoursesResponseDTO;
import br.com.crudcourses.v2.response.ListAllStudentsInCourseResponseDTO;
import br.com.crudcourses.v2.usecases.courses.CreateCourseUseCase;
import br.com.crudcourses.v2.usecases.courses.DeleteCoursesUseCase;
import br.com.crudcourses.v2.usecases.courses.ListAllCoursesUseCase;
import br.com.crudcourses.v2.usecases.courses.ListAllStudentsInCourse;
import br.com.crudcourses.v2.usecases.courses.UpdateCourseUseCase;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
@Tag(name = "Courses", description = "Endpoints for managing courses.")
public class CoursesControllerImpl implements CoursesController {

  private final CreateCourseUseCase createCourseUseCase;
  private final UpdateCourseUseCase updateCourseUseCase;
  private final DeleteCoursesUseCase deleteCoursesUseCase;
  private final ListAllStudentsInCourse listAllStudentsInCourse;
  private final ListAllCoursesUseCase listAllCoursesUseCase;
  private final CourseMapper courseMapper;

  @PutMapping("/{courseName}")
  @Override
  public ResponseEntity<CreateOrUpdateCourseResponseDTO> updateCourse(@RequestBody @Valid UpdateCourseDTO courseDTO, String courseName){
    var course = courseMapper.toCourse(courseDTO);
    var result = updateCourseUseCase.execute(course, courseName);
    var response = courseMapper.toResponse(result);
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  @DeleteMapping("/{courseName}")
  @Override
  public ResponseEntity<String> deleteCourse(String courseName){
    deleteCoursesUseCase.execute(courseName);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Course deleted successfully");
  }

  @GetMapping("/{courseName}/students")
  @Override
  public ResponseEntity<Page<ListAllStudentsInCourseResponseDTO>> listAllStudentsInCourse(
    @PathVariable String courseName,
    @RequestParam(defaultValue = "0") int page, 
    @RequestParam(defaultValue = "10") int size) {
    Page<Student> students = listAllStudentsInCourse.execute(courseName, page, size);
    Page<ListAllStudentsInCourseResponseDTO> response = courseMapper.toStudentPage(students);
    return ResponseEntity.ok(response);
  }

  @GetMapping
  @Override
  public ResponseEntity<Page<ListAllCoursesResponseDTO>> listAllCourses(
    @RequestParam(defaultValue = "0") int page, 
    @RequestParam(defaultValue = "10") int size) {
    Page<Courses> courses = listAllCoursesUseCase.execute(page, size);
    Page<ListAllCoursesResponseDTO> response = courseMapper.toCoursePage(courses);
    return ResponseEntity.ok(response);
  }
  
  @PostMapping
  @Override
  public ResponseEntity<CreateOrUpdateCourseResponseDTO> createCourse(@RequestBody @Valid RequestCreateCourseDTO courseDTO){
    var course = courseMapper.toCourse(courseDTO);
    var result = createCourseUseCase.execute(course);
    var response = courseMapper.toResponse(result);
    return ResponseEntity.status(HttpStatus.CREATED).body(response);
  }
}