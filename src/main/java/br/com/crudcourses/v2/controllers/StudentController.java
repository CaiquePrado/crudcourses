package br.com.crudcourses.v2.controllers;

import java.util.UUID;

import org.springframework.http.ResponseEntity;

import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.request.RequestCreateStudentDTO;
import br.com.crudcourses.v2.request.UpdateStudentOrTeacherDTO;
import br.com.crudcourses.v2.response.UpdateStudentOrTeacherResponseDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;

public interface StudentController {

  @ApiResponses(value = {
  @ApiResponse(responseCode = "201", 
  description = "Student created successfully", 
  content = @Content(schema = @Schema(implementation = Student.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Create a student")
  ResponseEntity<Student> createStudent(@Valid RequestCreateStudentDTO studentDTO);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "200", 
  description = "Student updated successfully", 
  content = @Content(schema = @Schema(implementation = UpdateStudentOrTeacherDTO.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Update a student")
  ResponseEntity<UpdateStudentOrTeacherResponseDTO> updateStudent(@Valid UpdateStudentOrTeacherDTO studentDTO, String cpf);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "201", 
  description = "Student added to course successfully", 
  content = @Content(schema = @Schema(implementation = Student.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Add a student to a course")
  ResponseEntity<Student> createStudentToCourse(UUID courseId, UUID studentId);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "200", 
  description = "Student found in course", 
  content = @Content(schema = @Schema(implementation = Boolean.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Check if a student is in a course")
  ResponseEntity<Boolean> isStudentInCourse(UUID courseId, UUID studentId);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "204", 
  description = "Student removed from course successfully"),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Remove a student from a course")
  ResponseEntity<Void> removeStudentFromCourse(UUID courseId, UUID studentId);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "200", 
  description = "Student activated successfully", 
  content = @Content(schema = @Schema(implementation = Student.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Activate a student")
  ResponseEntity<String> activateStudent(String cpf);
  
  @ApiResponses(value = {
  @ApiResponse(responseCode = "200", 
  description = "Student deactivated successfully"),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Deactivate a student")
  ResponseEntity<String> desactivateStudent(String cpf);

}
