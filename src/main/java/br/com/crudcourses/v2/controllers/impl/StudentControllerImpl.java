package br.com.crudcourses.v2.controllers.impl;

import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.crudcourses.v2.controllers.StudentController;
import br.com.crudcourses.v2.mapper.StudentMapper;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.request.RequestCreateStudentDTO;
import br.com.crudcourses.v2.request.UpdateStudentOrTeacherDTO;
import br.com.crudcourses.v2.response.UpdateStudentOrTeacherResponseDTO;
import br.com.crudcourses.v2.usecases.student.ActivateStudentUseCase;
import br.com.crudcourses.v2.usecases.student.CheckStudentInCourseUseCase;
import br.com.crudcourses.v2.usecases.student.CreateStudentToCourseUseCase;
import br.com.crudcourses.v2.usecases.student.CreateStudentUseCase;
import br.com.crudcourses.v2.usecases.student.DesactivateStudentUseCase;
import br.com.crudcourses.v2.usecases.student.RemoveStudentFromCourseUseCase;
import br.com.crudcourses.v2.usecases.student.UpdateStudentUseCase;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
@Tag(name = "Students", description = "Endpoints for managing students.")
public class StudentControllerImpl implements StudentController {

  private final CreateStudentUseCase createStudentUseCase;
  private final CreateStudentToCourseUseCase createStudentToCourse;
  private final CheckStudentInCourseUseCase checkStudentInCourseUseCase;
  private final RemoveStudentFromCourseUseCase removeStudentFromCourseUseCase;
  private final ActivateStudentUseCase activateStudentUseCase;
  private final DesactivateStudentUseCase desactiveStudentUseCase;
  private final UpdateStudentUseCase updateStudentUseCase;
  private final StudentMapper studentMapper;

  @PostMapping("/")
  @Override
  public ResponseEntity<Student> createStudent(@RequestBody @Valid RequestCreateStudentDTO studentDTO){
    var student = studentMapper.toStudent(studentDTO);
    var result = createStudentUseCase.execute(student);
    return ResponseEntity.status(HttpStatus.CREATED).body(result);
  }

  @PatchMapping("/{cpf}")
  @Override
  public ResponseEntity<UpdateStudentOrTeacherResponseDTO> updateStudent(@RequestBody @Valid UpdateStudentOrTeacherDTO studentDTO, @PathVariable String cpf){
    var student = studentMapper.toStudent(studentDTO);
    var result = updateStudentUseCase.execute(student, cpf);
    var response = new UpdateStudentOrTeacherResponseDTO(result.getName(), result.getAge());
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }

  @PostMapping("/{courseId}/{studentId}")
  @Override
  public ResponseEntity<Student> createStudentToCourse(@PathVariable UUID courseId, @PathVariable UUID studentId){
    var result = createStudentToCourse.execute(courseId, studentId);
    return ResponseEntity.status(HttpStatus.CREATED).body(result);
  }

  @GetMapping("/{courseId}/students/{studentId}")
  @Override
  public ResponseEntity<Boolean> isStudentInCourse(@PathVariable UUID courseId, @PathVariable UUID studentId) {
    boolean isStudentInCourse = checkStudentInCourseUseCase.execute(courseId, studentId);
    return ResponseEntity.ok(isStudentInCourse);
  }

  @DeleteMapping("/{courseId}/{studentId}")
  @Override
  public ResponseEntity<Void> removeStudentFromCourse(@PathVariable UUID courseId, @PathVariable UUID studentId){
    removeStudentFromCourseUseCase.execute(courseId, studentId);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

  @PatchMapping("/activate/{cpf}")
  @Override
  public ResponseEntity<String> activateStudent(@PathVariable String cpf){
    activateStudentUseCase.execute(cpf);
    return ResponseEntity.status(HttpStatus.OK).body("Student activated successfully");
  }

  @PatchMapping("/desactivate/{cpf}")
  @Override
  public ResponseEntity<String> desactivateStudent(@PathVariable String cpf){
    desactiveStudentUseCase.execute(cpf);
    return ResponseEntity.status(HttpStatus.OK).body("Student deactivated successfully");
  }
}
