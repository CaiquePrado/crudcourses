package br.com.crudcourses.v2.controllers;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.request.RequestCreateCourseDTO;
import br.com.crudcourses.v2.request.UpdateCourseDTO;
import br.com.crudcourses.v2.response.CreateOrUpdateCourseResponseDTO;
import br.com.crudcourses.v2.response.ListAllCoursesResponseDTO;
import br.com.crudcourses.v2.response.ListAllStudentsInCourseResponseDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;

public interface CoursesController {

  @ApiResponses(value = {
  @ApiResponse(responseCode = "200", 
  description = "Course updated successfully", 
  content = @Content(schema = @Schema(implementation = Courses.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Update a course")
  ResponseEntity<CreateOrUpdateCourseResponseDTO> updateCourse(@Valid UpdateCourseDTO courseDTO, String courseName);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "204", 
  description = "Course deleted successfully"),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Delete a course")
  ResponseEntity<String> deleteCourse(String courseName);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "200", 
  description = "Students found in course", 
  content = @Content(schema = @Schema(implementation = Page.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "List all students in a course")
  ResponseEntity<Page<ListAllStudentsInCourseResponseDTO>> listAllStudentsInCourse(String courseName, int page, int size);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "200", 
  description = "Courses retrieved successfully", 
  content = @Content(schema = @Schema(implementation = Page.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "List all courses")
  ResponseEntity<Page<ListAllCoursesResponseDTO>> listAllCourses(int page, int size);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "201", 
  description = "Course created successfully", 
  content = @Content(schema = @Schema(implementation = Courses.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Create a course")
  ResponseEntity<CreateOrUpdateCourseResponseDTO> createCourse(@Valid RequestCreateCourseDTO courseDTO);

}