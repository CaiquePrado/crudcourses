package br.com.crudcourses.v2.controllers.impl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.crudcourses.v2.controllers.TeacherController;
import br.com.crudcourses.v2.mapper.TeacherMapper;
import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.request.RequestCreateTeacherDTO;
import br.com.crudcourses.v2.request.UpdateStudentOrTeacherDTO;
import br.com.crudcourses.v2.response.UpdateStudentOrTeacherResponseDTO;
import br.com.crudcourses.v2.usecases.teacher.ActivateTeacherUseCase;
import br.com.crudcourses.v2.usecases.teacher.CreateTeacherUseCase;
import br.com.crudcourses.v2.usecases.teacher.DesactivateTeacherUseCase;
import br.com.crudcourses.v2.usecases.teacher.UpdateTeacherUseCase;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/teacher")
@RequiredArgsConstructor
@Tag(name = "Teachers", description = "Endpoints for managing teachers.")
public class TeacherControllerImpl implements TeacherController {

  private final CreateTeacherUseCase createTeacherUseCase;
  private final DesactivateTeacherUseCase desactiveTeacherUseCase;
  private final ActivateTeacherUseCase activateTeacherUseCase;
  private final UpdateTeacherUseCase updateTeacherUseCase;
  private final TeacherMapper teacherMapper;
  
  @PostMapping
  @Override
  public ResponseEntity<Teacher> createTeacher(@RequestBody @Valid RequestCreateTeacherDTO teacherDTO){
    var teacher = teacherMapper.toTeacher(teacherDTO);
    var result = createTeacherUseCase.execute(teacher);
    return ResponseEntity.status(HttpStatus.CREATED).body(result);
  }

  @PatchMapping("/activate/{cpf}")
  @Override
  public ResponseEntity<String> activateTeacher(@PathVariable String cpf){
    activateTeacherUseCase.execute(cpf);
    return ResponseEntity.status(HttpStatus.OK).body("Teacher activated successfully");
  }
  
  @PatchMapping("/desactivate/{cpf}")
  @Override
  public ResponseEntity<String> desactivateTeacher(@PathVariable String cpf){
    desactiveTeacherUseCase.execute(cpf);
    return ResponseEntity.status(HttpStatus.OK).body("Teacher deactivated successfully");
  }

  @PatchMapping("/{cpf}")
  @Override
  public ResponseEntity<UpdateStudentOrTeacherResponseDTO> updateTeacher(@RequestBody @Valid UpdateStudentOrTeacherDTO teacherDTO, @PathVariable String cpf){
    var teacher = teacherMapper.toTeacher(teacherDTO);
    var result = updateTeacherUseCase.execute(teacher, cpf);
    var response = new UpdateStudentOrTeacherResponseDTO(result.getName(), result.getAge());
    return ResponseEntity.status(HttpStatus.OK).body(response);
  }
} 