package br.com.crudcourses.v2.controllers;

import org.springframework.http.ResponseEntity;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.request.RequestCreateTeacherDTO;
import br.com.crudcourses.v2.request.UpdateStudentOrTeacherDTO;
import br.com.crudcourses.v2.response.UpdateStudentOrTeacherResponseDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;

public interface TeacherController {

  @ApiResponses(value = {
  @ApiResponse(responseCode = "201", 
  description = "Teacher created successfully", 
  content = @Content(schema = @Schema(implementation = Teacher.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Create a teacher")
  ResponseEntity<Teacher> createTeacher(@Valid RequestCreateTeacherDTO teacherDTO);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "200", 
  description = "Teacher activated successfully", 
  content = @Content(schema = @Schema(implementation = Teacher.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Activate a teacher")
  ResponseEntity<String> activateTeacher(String cpf);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "204", 
  description = "Teacher deactivated successfully"),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Deactivate a teacher")
  ResponseEntity<String> desactivateTeacher(String cpf);

  @ApiResponses(value = {
  @ApiResponse(responseCode = "200", 
  description = "Teacher updated successfully", 
  content = @Content(schema = @Schema(implementation = Teacher.class))),
  @ApiResponse(responseCode = "400", description = "Invalid request")})
  @Operation(description = "Update a teacher")
  ResponseEntity<UpdateStudentOrTeacherResponseDTO> updateTeacher(@Valid UpdateStudentOrTeacherDTO teacherDTO, String cpf);
  
}