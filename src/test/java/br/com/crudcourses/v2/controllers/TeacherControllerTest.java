package br.com.crudcourses.v2.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.request.RequestCreateTeacherDTO;
import br.com.crudcourses.v2.request.UpdateStudentOrTeacherDTO;
import br.com.crudcourses.v2.utils.SqlProvider;
import br.com.crudcourses.v2.utils.TeacherEntitiesBuilder;

@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest{

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper mapper;

  @Autowired
  TeacherRepository teacherRepository;

  String sendAsJson;
  RequestCreateTeacherDTO createTeacherDTOMock;
  RequestCreateTeacherDTO createTeacherDTOInvalidMock;

  @BeforeEach
  public void setup() throws JsonProcessingException {
      createTeacherDTOMock = TeacherEntitiesBuilder.validTeacherDTOBuilder();
      createTeacherDTOInvalidMock = TeacherEntitiesBuilder.invalidTeacherDTOBuilder();
      sendAsJson = mapper.writeValueAsString(createTeacherDTOMock);
  }
  
  @Test
  @DisplayName("Should be able to register a new teacher and return status 201") 
  void testgivenTeacherDto_WhenSave_ShouldCreateTeacherEndPointThenReturnStatus201() throws Exception {
      mockMvc
          .perform(MockMvcRequestBuilders.post("/teacher")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(sendAsJson))
          .andExpect(status().isCreated())
          .andExpect(jsonPath("$.name").value(createTeacherDTOMock.name()))
          .andExpect(jsonPath("$.age").value(createTeacherDTOMock.age()))
          .andExpect(jsonPath("$.cpf").value(createTeacherDTOMock.cpf()));

      assertTrue(teacherRepository.findByCpf(createTeacherDTOMock.cpf()).isPresent());
  }

  @Test
  @DisplayName("Should be able to register a new teacher and return status 400") 
  void testgivenWrongTeacherDto_WhenSave_ShouldReturnStatus400() throws Exception {
      sendAsJson = mapper.writeValueAsString(createTeacherDTOInvalidMock);
      mockMvc
          .perform(MockMvcRequestBuilders.post("/teacher")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(sendAsJson))
          .andExpect(status().isBadRequest());
    assertFalse(teacherRepository.findByCpf(createTeacherDTOMock.cpf()).isPresent());
  }

  @Test
  @DisplayName("Should not be possible to register a new teacher with an existing CPF and return status 400")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertTeachers),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  }) 
  void testgivenACreateTeacherDtoWithExistingCpf_WhenCreateTeacher_ShouldReturnStatus400() throws Exception {

    Teacher existingTeacher = teacherRepository.findAll().get(0);

    RequestCreateTeacherDTO duplicateCpfTeacher = TeacherEntitiesBuilder.duplicateCpfTeacherDTOBuilder(existingTeacher.getCpf());

    String json = mapper.writeValueAsString(duplicateCpfTeacher);

    mockMvc
        .perform(MockMvcRequestBuilders.post("/teacher")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
        .andExpect(status().isBadRequest());

    assertTrue(teacherRepository.findByCpf(duplicateCpfTeacher.cpf()).isPresent());
    Optional<Teacher> studentWithSameCpf = teacherRepository.findByCpf(duplicateCpfTeacher.cpf());
    assertTrue(studentWithSameCpf.isPresent());
    assertEquals(existingTeacher.getId(), studentWithSameCpf.get().getId());
  }

  @Test
  @DisplayName("Should be able to activate a teacher and return status 200")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertTeachers),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  }) 
  void testGivenTeacherId_WhenActivate_ShouldReturnStatus200() throws Exception {
  
     String cpf = "98765432109";
  
     MvcResult result = mockMvc
          .perform(MockMvcRequestBuilders.patch("/teacher/activate/" + cpf))
          .andExpect(status().isOk())
          .andReturn();
  
      String responseBody = result.getResponse().getContentAsString();
      assertTrue(responseBody.contains("Teacher activated successfully"));
      assertTrue(teacherRepository.findByCpf(cpf).get().getActive());
  }
  
  @Test
  @DisplayName("Should be able to deactivate a teacher and return status 200")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertTeachers),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  }) 
  void testGivenTeacherId_WhenDeactivate_ShouldReturnStatus200() throws Exception {
  
    String cpf = "12345678901";
  
     MvcResult result = mockMvc
          .perform(MockMvcRequestBuilders.patch("/teacher/desactivate/" + cpf))
          .andExpect(status().isOk())
          .andReturn();
  
      String responseBody = result.getResponse().getContentAsString();
      assertTrue(responseBody.contains("Teacher deactivated successfully"));
  }
  
  @Test
  @DisplayName("Should not be able to activate a teacher who is already active and return status 400")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertTeachers),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  }) 
  void testGivenActiveTeacherId_WhenActivate_ShouldReturnStatus400() throws Exception {
     
        String cpf = "12345678901";
  
        MvcResult result = mockMvc
          .perform(MockMvcRequestBuilders.patch("/teacher/activate/" + cpf))
          .andExpect(status().isBadRequest())
          .andReturn();
  
      String responseBody = result.getResponse().getContentAsString();
      assertTrue(responseBody.contains("The teacher is already active"));
      assertTrue(teacherRepository.findByCpf(cpf).get().getActive());
  }
  
  @Test
  @DisplayName("Should not be able to deactivate a teacher who is already inactive and return status 400")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertTeachers),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  }) 
  void testGivenInactiveTeacherId_WhenDeactivate_ShouldReturnStatus400() throws Exception {
  
    String cpf = "98765432109";
      
      mockMvc
          .perform(MockMvcRequestBuilders.patch("/teacher/desactivate/" + cpf))
          .andExpect(status().isBadRequest());
  
      assertFalse(teacherRepository.findByCpf(cpf).get().getActive());
  }

  @Test
  @DisplayName("Should be able to update a teacher and return status 200")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertTeachers),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  }) 
  void testGivenTeacherDtoAndId_WhenUpdate_ShouldReturnStatus200() throws Exception {

    String cpf = "12345678901";

    UpdateStudentOrTeacherDTO updateTeacherDTO = TeacherEntitiesBuilder.updateTeacherDTOBuilder();

    String json = mapper.writeValueAsString(updateTeacherDTO);

    mockMvc
        .perform(MockMvcRequestBuilders.patch("/teacher/" + cpf)
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
        .andExpect(status().isOk());

    Teacher updatedTeacher = teacherRepository.findByCpf(cpf).get();
    assertEquals("Updated Name", updatedTeacher.getName());
    assertEquals(22, updatedTeacher.getAge());
  }
}
