package br.com.crudcourses.v2.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.request.RequestCreateCourseDTO;
import br.com.crudcourses.v2.utils.CoursesEntitiesBuilder;
import br.com.crudcourses.v2.utils.SqlProvider;

@SpringBootTest
@AutoConfigureMockMvc
class CoursesControllerTest {
  
  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper mapper;

  @Autowired
  CoursesRepository coursesRepository;

  String sendAsJson;
  RequestCreateCourseDTO createCourseDTOMock;
  RequestCreateCourseDTO createCourseDTOInvalidMock;

  @BeforeEach
  public void setup() throws JsonProcessingException {
      createCourseDTOMock = CoursesEntitiesBuilder.validCourseDTOBuilder();
      createCourseDTOInvalidMock = CoursesEntitiesBuilder.invalidCourseDTOBuilder();
      sendAsJson = mapper.writeValueAsString(createCourseDTOMock);
  }

  @Test
  @DisplayName("Should be able to register a new course and return status 201")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertTeachers),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  }) 
  void testGivenCourseDto_WhenSave_ShouldCreateCourseEndPointThenReturnStatus201() throws Exception {

   mockMvc
          .perform(MockMvcRequestBuilders.post("/courses")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(sendAsJson))
          .andExpect(status().isCreated())
          .andExpect(jsonPath("$.courseName").value(createCourseDTOMock.courseName()));
  }

  @Test
  @DisplayName("Should not be able to register a new course with invalid data and return status 400")
  void testGivenInvalidCourseDto_WhenSave_ShouldReturnBadRequest() throws Exception {

    sendAsJson = mapper.writeValueAsString(createCourseDTOInvalidMock);
      mockMvc
          .perform(MockMvcRequestBuilders.post("/courses")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(sendAsJson))
          .andExpect(status().isBadRequest());

  }
  
  @Test
  @DisplayName("Should not be able to delete a non-existent course and return status 400")
  void testGivenInvalidCourseId_WhenDelete_ShouldReturnNotContent() throws Exception {

    String invalidCouseName = "dsadasdawdawdawdwdawda";

    mockMvc
        .perform(MockMvcRequestBuilders.delete("/courses/" + invalidCouseName))
        .andExpect(status().isBadRequest());
  }

@Test
@DisplayName("Should be able to list all courses and return status 200")
void testGivenValidPageAndSize_WhenListAllCourses_ShouldReturnStatus200() throws Exception {

  int page = 0;
  int size = 10;

  mockMvc
      .perform(MockMvcRequestBuilders.get("/courses")
          .param("page", String.valueOf(page))
          .param("size", String.valueOf(size))
          .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk());
  }

@Test
@SqlGroup({
  @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertTeachers),
  @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertCourses),
  @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
}) 
@DisplayName("Should be able to list all students in a course and return status 200")
void testGivenValidCourseIdAndPageAndSize_WhenListAllStudentsInCourse_ShouldReturnStatus200() throws Exception {

  String courseName = "matematica";
  int page = 0;
  int size = 10;

  mockMvc
      .perform(MockMvcRequestBuilders.get("/courses/" + courseName + "/students")
          .param("page", String.valueOf(page))
          .param("size", String.valueOf(size))
          .contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk());
  }
}
