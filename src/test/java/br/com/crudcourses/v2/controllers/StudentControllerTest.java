package br.com.crudcourses.v2.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.request.RequestCreateStudentDTO;
import br.com.crudcourses.v2.request.UpdateStudentOrTeacherDTO;
import br.com.crudcourses.v2.utils.SqlProvider;
import br.com.crudcourses.v2.utils.StudentEntitiesBuilder;

@SpringBootTest
@AutoConfigureMockMvc
class StudentControllerTest{

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper mapper;

  @Autowired
  StudentRepository studentRepository;

  String sendAsJson;
  RequestCreateStudentDTO createStudentDTOMock;
  RequestCreateStudentDTO createStudentDTOInvalidMock;

  @BeforeEach
  public void setup() throws JsonProcessingException {
      createStudentDTOMock = StudentEntitiesBuilder.validStudentDTOBuilder();
      createStudentDTOInvalidMock = StudentEntitiesBuilder.invalidStudentDTOBuilder();
      sendAsJson = mapper.writeValueAsString(createStudentDTOMock);
  }
  
  @Test
  @DisplayName("Should be able to register a new student and return status 201") 
  void testgivenStudentDto_WhenSave_ShouldCreateStudentEndPointThenReturnStatus201() throws Exception {
      mockMvc
          .perform(MockMvcRequestBuilders.post("/student/")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(sendAsJson))
          .andExpect(status().isCreated())
          .andExpect(jsonPath("$.name").value(createStudentDTOMock.name()))
          .andExpect(jsonPath("$.age").value(createStudentDTOMock.age()))
          .andExpect(jsonPath("$.cpf").value(createStudentDTOMock.cpf()));

      assertTrue(studentRepository.findByCpf(createStudentDTOMock.cpf()).isPresent());
  }

  @Test
  @DisplayName("Should be able to register a new student and return status 400") 
  void testgivenWrongStudentDto_WhenSave_ShouldReturnStatus400() throws Exception {
      sendAsJson = mapper.writeValueAsString(createStudentDTOInvalidMock);
      mockMvc
          .perform(MockMvcRequestBuilders.post("/student/")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(sendAsJson))
          .andExpect(status().isBadRequest());

      assertFalse(studentRepository.findByCpf(createStudentDTOMock.cpf()).isPresent());
  }

  @Test
  @DisplayName("Should not be possible to register a new student with an existing CPF and return status 400")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertStutends),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  }) 
  void testgivenACreateStudentDtoWithExistingCpf_WhenCreateStudent_ShouldReturnStatus400() throws Exception {

    Student existingStudent = studentRepository.findAll().get(0);

    RequestCreateStudentDTO duplicateCpfStudent = StudentEntitiesBuilder.duplicateCpfStudentDTOBuilder(existingStudent.getCpf());

    String json = mapper.writeValueAsString(duplicateCpfStudent);

    mockMvc
        .perform(MockMvcRequestBuilders.post("/student/")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
        .andExpect(status().isBadRequest());

    Optional<Student> studentWithSameCpf = studentRepository.findByCpf(duplicateCpfStudent.cpf());
    assertTrue(studentWithSameCpf.isPresent());
    assertEquals(existingStudent.getId(), studentWithSameCpf.get().getId());
  }

  @Test
  @DisplayName("Should be able to activate a student and return status 200")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertStutends),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  })  
  void testGivenStudentId_WhenActivate_ShouldReturnStatus200() throws Exception {

    String cpf = "51749469073";

    MvcResult result = mockMvc
      .perform(MockMvcRequestBuilders.patch("/student/activate/" + cpf))
      .andExpect(status().isOk())
      .andReturn();

    String responseBody = result.getResponse().getContentAsString();
    assertTrue(responseBody.contains("Student activated successfully"));

    assertTrue(studentRepository.findByCpf(cpf).get().getActive());
}

  @Test
  @DisplayName("Should be able to deactivate a student and return status 200")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertStutends),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  })  
  void testGivenStudentId_WhenDesactivate_ShouldReturnStatus200() throws Exception {

    String cpf = "91368318088";

    MvcResult result = mockMvc
      .perform(MockMvcRequestBuilders.patch("/student/desactivate/" + cpf))
      .andExpect(status().isOk())
      .andReturn();

    String responseBody = result.getResponse().getContentAsString();
    assertTrue(responseBody.contains("Student deactivated successfully"));
    assertFalse(studentRepository.findByCpf(cpf).get().getActive());
  }

  @Test
  @DisplayName("Should not be able to activate a student who is already active and return status 400")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertStutends),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  })  
  void testGivenActiveStudentId_WhenActivate_ShouldReturnStatus400() throws Exception {
   
    String cpf = "91368318088";

    MvcResult result = mockMvc
      .perform(MockMvcRequestBuilders.patch("/student/activate/" + cpf))
      .andExpect(status().isBadRequest())
      .andReturn();

    String responseBody = result.getResponse().getContentAsString();
    assertTrue(responseBody.contains("The Student is already active"));
    assertTrue(studentRepository.findByCpf(cpf).get().getActive());
}

  @Test
  @DisplayName("Should not be able to deactivate a student who is already inactive and return status 400")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertStutends),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  }) 
  void testGivenInactiveStudentId_WhenDesactivate_ShouldReturnStatus400() throws Exception {

    String cpf = "51749469073";

    assertFalse(studentRepository.findByCpf(cpf).get().getActive());
  
    mockMvc
      .perform(MockMvcRequestBuilders.delete("/student/desactivate/" + cpf))
      .andExpect(status().isBadRequest())
      .andReturn();
  }

  @Test
  @DisplayName("Should be able to update a student and return status 200")
  @SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = SqlProvider.insertStutends),
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = SqlProvider.clearDB)
  })  
  void testGivenStudentDtoAndId_WhenUpdate_ShouldReturnStatus200() throws Exception {

    String cpf = "59364813014";

    UpdateStudentOrTeacherDTO updateStudentDTO = StudentEntitiesBuilder.updateStudentDTOBuilder();

    String json = mapper.writeValueAsString(updateStudentDTO);

    mockMvc
        .perform(MockMvcRequestBuilders.patch("/student/" + cpf)
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
        .andExpect(status().isOk());

    Student updatedStudent = studentRepository.findByCpf(cpf).get();
    assertEquals("Updated Name", updatedStudent.getName());
    assertEquals(22, updatedStudent.getAge());
  }
}


