package br.com.crudcourses.v2.usecases.courses;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.impl.DeleteCoursesUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class DeleteCoursesUseCaseTest {

  @Mock
  CoursesRepository coursesRepository;

  @Mock
  FindCourseByCourseNameUseCase findCourseByNameUseCase;

  @InjectMocks
  DeleteCoursesUseCaseImpl deleteCoursesUseCaseImpl;

  UUID id;
  Courses course;

  @BeforeEach
  void setup() {
    id = UUID.randomUUID();
    course = Courses.builder()
                    .id(id)
                    .courseName("Mathematics")
                    .students(new ArrayList<>())
                    .build();
  }

  @Test
  void testGivenValidId_WhenDeleteCourse_ShouldNotThrow() {
    given(findCourseByNameUseCase.find(course.getCourseName())).willReturn(course);

    assertDoesNotThrow(() -> deleteCoursesUseCaseImpl.execute(course.getCourseName()));

    verify(coursesRepository).delete(course);
  }

  @Test
  void testGivenInvalidId_WhenDeleteCourse_ShouldThrowResourceNotFoundException() {
    String invalidCourseName = "";
    given(findCourseByNameUseCase.find(invalidCourseName)).willThrow(new InvalidRequestException(null));

    assertThrows(InvalidRequestException.class, () -> {
      deleteCoursesUseCaseImpl.execute(invalidCourseName);
    });

    verify(coursesRepository, never()).delete(any(Courses.class));
  }
}

