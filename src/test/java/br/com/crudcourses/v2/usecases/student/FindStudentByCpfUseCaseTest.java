package br.com.crudcourses.v2.usecases.student;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.ResourceNotFoundException;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.impl.FindStudentByCpfUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class FindStudentByCpfUseCaseTest {
  
  @Mock
  StudentRepository studentRepository;

  @InjectMocks
  FindStudentByCpfUseCaseImpl findStudentByCpfUseCaseImpl;

  Student student;
  UUID studentId;

  @BeforeEach
  void setup() {
    studentId = UUID.randomUUID();
    student = Student.builder()
                    .id(studentId)
                    .name("John Doe")
                    .age(25)
                    .cpf("73108634020")
                    .build();
  }


  @Test
  void testGivenValidCpf_WhenFindStudentByCpf_ShouldReturnStudent() {

    given(studentRepository.findByCpf(student.getCpf())).willReturn(Optional.of(student));

    Student foundStudent = findStudentByCpfUseCaseImpl.find(student.getCpf());

    assertNotNull(foundStudent);
    assertEquals(student.getCpf(), foundStudent.getCpf());
    assertEquals(student.getName(), foundStudent.getName());
    assertEquals(student.getAge(), foundStudent.getAge());
  }

  @Test
  void testGivenInvalidCpf_WhenFindStudentByCpf_ShouldThrowResourceNotFoundException() {
    
    given(studentRepository.findByCpf(student.getCpf())).willReturn(Optional.empty());

    assertThrows(ResourceNotFoundException.class, () -> {
      findStudentByCpfUseCaseImpl.find(student.getCpf());
    });
  }
}

