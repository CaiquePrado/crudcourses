package br.com.crudcourses.v2.usecases.student;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.impl.DesactivateStudentUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class DesactiveStudentUseCaseTest {

  @Mock
  FindStudentByCpfUseCase findStudentByCpfUseCase;

  @Mock
  StudentRepository studentRepository;

  @InjectMocks
  DesactivateStudentUseCaseImpl desactiveStudentUseCaseImpl;
  
  Student existingStudent;
  UUID id;

  @BeforeEach
  void setup() {
    id = UUID.randomUUID();
    existingStudent = Student.builder()
                             .id(id)
                             .name("John")
                             .age(30)
                             .cpf("10512971601")
                             .active(true)
                             .build();
  }

  @Test
  void testGivenValidId_WhenDesactiveStudent_ShouldSetActiveToFalse() {
    given(findStudentByCpfUseCase.find(existingStudent.getCpf())).willReturn(existingStudent);

    desactiveStudentUseCaseImpl.execute(existingStudent.getCpf());

    assertFalse(existingStudent.getActive());
    verify(studentRepository).save(existingStudent);
  }

  @Test
  void testGivenAlreadyDesactivatedStudent_WhenDesactiveStudent_ShouldThrowException() {
  Student alreadyDesactivatedStudent = Student.builder()
                                               .id(id)
                                               .name("John")
                                               .age(30)
                                               .cpf("10512971601")
                                               .active(false)
                                               .build();
    given(findStudentByCpfUseCase.find(existingStudent.getCpf())).willReturn(alreadyDesactivatedStudent);

    assertThrows(InvalidRequestException.class, () -> {
      desactiveStudentUseCaseImpl.execute(existingStudent.getCpf());
    });
  }
}

