package br.com.crudcourses.v2.usecases.teacher;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.impl.DesactivateTeacherUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class DesactiveTeacherUseCaseTest {

  @Mock
  TeacherRepository teacherRepository;

  @Mock
  FindTeacherByCpfUseCase findTeacherByCpfUseCase;

  @InjectMocks
  DesactivateTeacherUseCaseImpl desactivateTeacherUseCaseImpl;

  Teacher existingTeacher;
  UUID id;

  @BeforeEach
  void setup() {
    id = UUID.randomUUID();
    existingTeacher = Teacher.builder()
                             .id(id)
                             .name("John")
                             .age(30)
                             .cpf("10512971601")
                             .workCard("WC123")
                             .salary(5000.00)
                             .active(true)
                             .build();
  }

  @Test
  void testGivenValidId_WhenDesactiveTeacher_ShouldSetActiveToFalse() {
    
    given(findTeacherByCpfUseCase.find(existingTeacher.getCpf())).willReturn(existingTeacher);

    desactivateTeacherUseCaseImpl.execute(existingTeacher.getCpf());

    assertFalse(existingTeacher.getActive());
    verify(teacherRepository).save(existingTeacher);
  }

  @Test
  void testGivenAlreadyDesactivatedTeacher_WhenDesactiveTeacher_ShouldThrowException() {
    Teacher alreadyDeactivatedTeacher = Teacher.builder()
                                               .id(id)
                                               .name("John")
                                               .age(30)
                                               .cpf("10512971601")
                                               .workCard("WC123")
                                               .salary(5000.00)
                                               .active(false)
                                               .build();

    given(findTeacherByCpfUseCase.find(existingTeacher.getCpf())).willReturn(alreadyDeactivatedTeacher);

    assertThrows(InvalidRequestException.class, () -> {
    desactivateTeacherUseCaseImpl.execute(existingTeacher.getCpf());
    });
  }
}

