package br.com.crudcourses.v2.usecases.courses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.impl.UpdateCourseUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class UpdateCourseUseCaseTest {

  @Mock
  CoursesRepository coursesRepository;

  @Mock
  FindCourseByCourseNameUseCase findCourseByNameUseCase;

  @InjectMocks
  UpdateCourseUseCaseImpl updateCourseUseCaseImpl;

  Courses existingCourse;
  Courses updatedCourse;
  UUID id;

  @BeforeEach
  void setup() {
    id = UUID.randomUUID();

    existingCourse = Courses.builder()
                            .id(id)
                            .courseName("Physics")
                            .students(new ArrayList<>())
                            .build();

    updatedCourse = Courses.builder()
                           .id(id)
                           .courseName("Mathematics")
                           .students(new ArrayList<>())
                           .build();
  }

  @Test
  void testGivenCourseDTO_WhenUpdateCourse_ShouldReturnUpdatedCourse() {
    given(findCourseByNameUseCase.find(updatedCourse.getCourseName())).willReturn(existingCourse);
    given(coursesRepository.save(any(Courses.class))).willAnswer(invocation -> invocation.getArgument(0));
  
    Courses result = updateCourseUseCaseImpl.execute(updatedCourse, updatedCourse.getCourseName());
  
    assertNotNull(result);
    assertEquals(updatedCourse.getCourseName(), result.getCourseName());
  }

  @Test
  void testGivenInvalidId_WhenUpdateCourse_ShouldThrowIllegalArgumentException() {
    String invalidCourseName = "";
    given(findCourseByNameUseCase.find(invalidCourseName)).willThrow(new IllegalArgumentException());

    assertThrows(IllegalArgumentException.class, () -> {
      updateCourseUseCaseImpl.execute(updatedCourse, invalidCourseName);
    });

    verify(coursesRepository, never()).save(any(Courses.class));
  }
}

