package br.com.crudcourses.v2.usecases.student;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.request.RequestCreateStudentDTO;
import br.com.crudcourses.v2.usecases.student.impl.CreateStudentUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class CreateStudentUseCaseTest {
  
  @Mock
  StudentRepository studentRepository;

  @InjectMocks
  CreateStudentUseCaseImpl createStudentUseCaseImpl;

  RequestCreateStudentDTO createStudentDTO;
  Student student;

  @BeforeEach
  void setup(){
    student = Student.builder()
                     .name("Roger")
                     .age(20)
                     .cpf("10512971601")
                     .build();
  }

  @DisplayName("Given Teacher Object when Save Student should Return Student")
  @Test
  void testGivenStudentObject_WhenSaveStudent_ShouldReturnStudent() {

    given(studentRepository.save(any(Student.class))).willAnswer(invocation -> invocation.getArgument(0));

    Student savedStudent = createStudentUseCaseImpl.execute(student);

    assertNotNull(savedStudent);
    assertEquals(student.getName(), savedStudent.getName());
    assertEquals(student.getAge(), savedStudent.getAge());
    assertEquals(student.getCpf(), savedStudent.getCpf());
  }

  @DisplayName("Given Student Object with Existing CPF when Save Student should Throw DataIntegrityViolationException")
  @Test
  void testGivenStudentObjectWithExistingCPF_WhenSaveStudent_ShouldThrowDataIntegrityViolationException() {

    given(studentRepository.findByCpf(anyString())).willReturn(Optional.of(student));

    assertThrows(InvalidRequestException.class, () -> {
      createStudentUseCaseImpl.execute(student);
    });

    verify(studentRepository, never()).save(any(Student.class));
  }
}
