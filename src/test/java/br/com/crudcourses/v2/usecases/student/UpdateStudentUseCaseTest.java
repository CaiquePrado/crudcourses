package br.com.crudcourses.v2.usecases.student;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.impl.UpdateStudentUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class UpdateStudentUseCaseTest {

  @Mock
  StudentRepository studentRepository;

  @Mock
  FindStudentByCpfUseCase findStudentByCpfUseCase;

  @InjectMocks
  UpdateStudentUseCaseImpl updateStudentUseCaseImpl;

  Student existingStudent;
  Student updatedStudent;
  String cpf;

  @BeforeEach
  void setup() {
    cpf = "10512971601";

    existingStudent = Student.builder()
                    .id(UUID.randomUUID())
                    .name("John")
                    .age(30)
                    .cpf(cpf)
                    .build();
    updatedStudent = Student.builder()
                    .id(UUID.randomUUID())
                    .name("Roger")
                    .age(25)
                    .cpf(cpf)
                    .build();
  }

  @Test
  void testGivenStudentObject_WhenUpdateStudent_ShouldReturnUpdatedStudent() {

    given(findStudentByCpfUseCase.find(cpf)).willReturn(existingStudent);
    given(studentRepository.save(any(Student.class))).willAnswer(invocation -> invocation.getArgument(0));

    Student result = updateStudentUseCaseImpl.execute(updatedStudent, cpf);

    assertNotNull(result);
    assertEquals(updatedStudent.getName(), result.getName());
    assertEquals(updatedStudent.getAge(), result.getAge());
  }

  @Test
  void testGivenInvalidCpf_WhenUpdateStudent_ShouldThrowIllegalArgumentException() {

    String invalidCpf = "12345678901";
    given(findStudentByCpfUseCase.find(invalidCpf)).willThrow(new IllegalArgumentException());

    assertThrows(IllegalArgumentException.class, () -> {
      updateStudentUseCaseImpl.execute(updatedStudent, invalidCpf);
    });

    verify(studentRepository, never()).save(any(Student.class));
  }
}
