package br.com.crudcourses.v2.usecases.courses;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.impl.ListAllStudentsInCourseImpl;

@ExtendWith(MockitoExtension.class)
class ListAllStudentsInCourseTest {

  @Mock
  CoursesRepository coursesRepository;

  @Mock
  FindCourseByCourseNameUseCase findCourseByNameUseCase;

  @InjectMocks
  ListAllStudentsInCourseImpl listAllStudentsInCourseImpl;

  UUID courseId;
  Courses course;
  Pageable pageable;
  Page<Student> studentsPage;

  @BeforeEach
  void setup() {
    courseId = UUID.randomUUID();
    course = Courses.builder()
                    .id(courseId)
                    .courseName("Mathematics")
                    .students(new ArrayList<>())
                    .build();
    pageable = PageRequest.of(0, 10);
    studentsPage = new PageImpl<>(course.getStudents(), pageable, course.getStudents().size());
  }

  @Test
  void testGivenValidCourseId_WhenListAllStudentsInCourse_ShouldReturnPageOfStudents() {
    given(findCourseByNameUseCase.find(course.getCourseName())).willReturn(course);
    given(coursesRepository.findActiveStudentsByCourseName(course.getCourseName(), pageable)).willReturn(studentsPage);

    Page<Student> result = listAllStudentsInCourseImpl.execute(course.getCourseName(), 0, 10);

    assertNotNull(result);
    assertEquals(course.getStudents().size(), result.getNumberOfElements());
  }

  @Test
  void testGivenInvalidCourseId_WhenListAllStudentsInCourse_ShouldThrowIllegalArgumentException() {
    String invalidCourseName = "";
    given(findCourseByNameUseCase.find(invalidCourseName)).willThrow(new IllegalArgumentException());

    assertThrows(IllegalArgumentException.class, () -> {
      listAllStudentsInCourseImpl.execute(invalidCourseName, 0, 10);
    });
  }
}


