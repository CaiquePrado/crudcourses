package br.com.crudcourses.v2.usecases.teacher;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.impl.FindTeacherByWorkCardUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class FindTacherByWorkCardUseCaseTest {
  
  @Mock
  TeacherRepository teacherRepository;

  @InjectMocks
  FindTeacherByWorkCardUseCaseImpl findTeacherByWorkCardUseCaseImpl;

  Teacher teacher;
  UUID teacherId;

  @BeforeEach
  void setup() {
    teacherId = UUID.randomUUID();
    teacher = Teacher.builder()
                    .id(teacherId)
                    .name("Roger")
                    .age(20)
                    .cpf("10512971601")
                    .workCard("WC123")
                    .salary(5000.00)
                    .build();
  }


  @Test
  void testGivenValidCpf_WhenFindTeacherByWorkCard_ShouldReturnTeacher() {

    given(teacherRepository.findByWorkCard(teacher.getWorkCard())).willReturn(Optional.of(teacher));

    Teacher foundTeacher = findTeacherByWorkCardUseCaseImpl.find(teacher.getWorkCard());

    assertNotNull(foundTeacher);
    assertEquals(teacher.getWorkCard(), foundTeacher.getWorkCard());
    assertEquals(teacher.getName(), foundTeacher.getName());
    assertEquals(teacher.getAge(), foundTeacher.getAge());
  }

  @Test
  void testGivenInvalidCpf_WhenFindTeacherByWorkCard_ShouldThrowIllegalArgumentException() {

    given(teacherRepository.findByWorkCard(teacher.getWorkCard())).willReturn(Optional.empty());

    assertThrows(IllegalArgumentException.class, () -> {
      findTeacherByWorkCardUseCaseImpl.find(teacher.getWorkCard());
    });
  }
}

