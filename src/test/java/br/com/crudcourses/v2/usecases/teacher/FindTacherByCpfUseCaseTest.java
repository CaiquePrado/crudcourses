package br.com.crudcourses.v2.usecases.teacher;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.impl.FindTeacherByCpfUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class FindTacherByCpfUseCaseTest {
  
  @Mock
  TeacherRepository teacherRepository;

  @InjectMocks
  FindTeacherByCpfUseCaseImpl findTeacherByCpfUseCaseImpl;

  Teacher teacher;
  UUID teacherId;

  @BeforeEach
  void setup() {
    teacherId = UUID.randomUUID();
    teacher = Teacher.builder()
                    .id(teacherId)
                    .name("Roger")
                    .age(20)
                    .cpf("10512971601")
                    .workCard("WC123")
                    .salary(5000.00)
                    .build();
  }


  @Test
  void testGivenValidCpf_WhenFindTeacherByCpf_ShouldReturnTeacher() {

    given(teacherRepository.findByCpf(teacher.getCpf())).willReturn(Optional.of(teacher));

    Teacher foundTeacher = findTeacherByCpfUseCaseImpl.find(teacher.getCpf());

    assertNotNull(foundTeacher);
    assertEquals(teacher.getCpf(), foundTeacher.getCpf());
    assertEquals(teacher.getName(), foundTeacher.getName());
    assertEquals(teacher.getAge(), foundTeacher.getAge());
  }

  @Test
  void testGivenInvalidCpf_WhenFindTeacherByCpf_ShouldThrowIllegalArgumentException() {

    given(teacherRepository.findByCpf(teacher.getCpf())).willReturn(Optional.empty());

    assertThrows(IllegalArgumentException.class, () -> {
      findTeacherByCpfUseCaseImpl.find(teacher.getCpf());
    });
  }
}

