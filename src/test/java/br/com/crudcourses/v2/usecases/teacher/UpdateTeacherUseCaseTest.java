package br.com.crudcourses.v2.usecases.teacher;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.impl.UpdateTeacherUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class UpdateTeacherUseCaseTest {

  @Mock
  TeacherRepository teacherRepository;

  @Mock
  FindTeacherByCpfUseCase findTeacherByCpfUseCase;

  @InjectMocks
  UpdateTeacherUseCaseImpl updateTeacherUseCaseImpl;

  UUID id;
  Teacher existingTeacher;
  Teacher updatedTeacher;

  @BeforeEach
  void setup() {
    id = UUID.randomUUID();
    existingTeacher = Teacher.builder()
                    .id(id)
                    .name("John")
                    .age(30)
                    .cpf("10512971601")
                    .workCard("WC123")
                    .salary(5000.00)
                    .build();
    updatedTeacher = Teacher.builder()
                    .id(id)
                    .name("Pedro")
                    .age(24)
                    .build();
  }

  @Test
  void testGivenTeacherObject_WhenUpdateTeacher_ShouldReturnUpdatedTeacher() {

    given(findTeacherByCpfUseCase.find(existingTeacher.getCpf())).willReturn(existingTeacher);
    given(teacherRepository.save(any(Teacher.class))).willAnswer(invocation -> invocation.getArgument(0));

    Teacher result = updateTeacherUseCaseImpl.execute(updatedTeacher, existingTeacher.getCpf());

    assertNotNull(result);
    assertEquals(updatedTeacher.getName(), result.getName());
    assertEquals(updatedTeacher.getAge(), result.getAge());
    verify(findTeacherByCpfUseCase).find(existingTeacher.getCpf());
  }

  @Test
  void testGivenInvalidId_WhenUpdateTeacher_ShouldThrowIllegalArgumentException() {

    String invalidCpf = "432i4ussfjsdbfu523u4";
    given(findTeacherByCpfUseCase.find(invalidCpf)).willThrow(new IllegalArgumentException());

    assertThrows(IllegalArgumentException.class, () -> {
      updateTeacherUseCaseImpl.execute(updatedTeacher, invalidCpf);
    });
    
    verify(findTeacherByCpfUseCase).find(invalidCpf);
    verify(teacherRepository, never()).save(any(Teacher.class));
  }
}
