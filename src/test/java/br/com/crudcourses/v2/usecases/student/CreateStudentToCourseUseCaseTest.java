package br.com.crudcourses.v2.usecases.student;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.FindCourseByIdUseCase;
import br.com.crudcourses.v2.usecases.student.impl.CreateStudentToCourseImpl;

@ExtendWith(MockitoExtension.class)
class CreateStudentToCourseUseCaseTest {

  @Mock
  CoursesRepository courseRepository;

  @Mock
  FindStudentByIdUseCase findStudentByIdUseCase;

  @Mock
  FindCourseByIdUseCase findCourseByIdUseCase;

  @InjectMocks
  CreateStudentToCourseImpl createStudentToCourseUseCase;

  UUID courseId;
  UUID studentId;
  Courses course;
  Student student;

  @BeforeEach
  void setup() {
    courseId = UUID.randomUUID();
    studentId = UUID.randomUUID();

    student = Student.builder()
                   .id(studentId)
                   .name("Roger")
                   .age(20)
                   .cpf("10512971501")
                   .courses(new ArrayList<>())
                   .createdAt(LocalDateTime.now())
                   .active(true)
                   .build();

    course = Courses.builder()
                    .id(courseId)
                    .courseName("Mathematics")
                    .students(new ArrayList<>())
                    .createdAt(LocalDateTime.now())
                    .build();
  }

  @Test
  @DisplayName("Given Course and Student IDs when Add Student to Course should Return Student")
  void shouldAddStudentToCourse() {
    given(findCourseByIdUseCase.find(courseId)).willReturn(course);
    given(findStudentByIdUseCase.find(studentId)).willReturn(student);

    Student addedStudent = createStudentToCourseUseCase.execute(courseId, studentId);

    assertTrue(course.getStudents().contains(student));
    assertEquals(student, addedStudent);
    verify(courseRepository, times(1)).save(course);
  }

  @Test
  @DisplayName("Given invalid Course ID when Add Student to Course should Throw Exception")
  void shouldThrowExceptionWhenCourseIdIsInvalid() {
    given(findCourseByIdUseCase.find(courseId)).willThrow(new IllegalArgumentException());

    assertThrows(IllegalArgumentException.class, () -> {
      createStudentToCourseUseCase.execute(courseId, studentId);
    });

    verifyNoInteractions(courseRepository);
  }

  @Test
  @DisplayName("Given invalid Student ID when Add Student to Course should Throw Exception")
  void shouldThrowExceptionWhenStudentIdIsInvalid() {

    given(findCourseByIdUseCase.find(courseId)).willReturn(course);
    given(findStudentByIdUseCase.find(studentId)).willThrow(new IllegalArgumentException());

    assertThrows(IllegalArgumentException.class, () -> {
      createStudentToCourseUseCase.execute(courseId, studentId);
    });

    verifyNoInteractions(courseRepository);
  }
}


