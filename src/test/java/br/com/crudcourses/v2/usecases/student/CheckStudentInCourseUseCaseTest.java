package br.com.crudcourses.v2.usecases.student;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.usecases.courses.FindCourseByIdUseCase;
import br.com.crudcourses.v2.usecases.student.impl.CheckStudentInCourseUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class CheckStudentInCourseUseCaseTest {

  @Mock
  FindCourseByIdUseCase findCourseByIdUseCase;

  @Mock
  FindStudentByIdUseCase findStudentByIdUseCase;

  @InjectMocks
  CheckStudentInCourseUseCaseImpl checkStudentInCourseUseCaseImpl;

  UUID courseId;
  UUID studentId;
  Courses course;
  Student student;

  @BeforeEach
  void setup() {
    courseId = UUID.randomUUID();
    studentId = UUID.randomUUID();
    student = Student.builder().id(studentId).build();
    course = Courses.builder()
                    .id(courseId)
                    .students(Arrays.asList(student))
                    .build();
  }

  @Test
  void testGivenCourseIdAndStudentId_WhenCheckStudentInCourse_ShouldReturnTrue() {
    given(findCourseByIdUseCase.find(courseId)).willReturn(course);
    given(findStudentByIdUseCase.find(studentId)).willReturn(student);

    boolean isStudentInCourse = checkStudentInCourseUseCaseImpl.execute(courseId, studentId);

    assertTrue(isStudentInCourse);
  }

  @Test
  void testGivenCourseIdAndStudentId_WhenCheckStudentNotInCourse_ShouldReturnFalse() {
    UUID invalidStudentId = UUID.randomUUID();
    given(findCourseByIdUseCase.find(courseId)).willReturn(course);
    given(findStudentByIdUseCase.find(invalidStudentId)).willThrow(new IllegalArgumentException("No student found with ID " + invalidStudentId));

    assertThrows(IllegalArgumentException.class, () -> checkStudentInCourseUseCaseImpl.execute(courseId, invalidStudentId));
  }
}


