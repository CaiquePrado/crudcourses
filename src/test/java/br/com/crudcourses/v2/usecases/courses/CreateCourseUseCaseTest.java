package br.com.crudcourses.v2.usecases.courses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.impl.CreateCourseUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class CreateCourseUseCaseTest {

  @Mock
  CoursesRepository coursesRepository;

  @InjectMocks
  CreateCourseUseCaseImpl createCourseUseCaseImpl;

  Courses course;

  @BeforeEach
  void setup() {
    course = Courses.builder()
                    .courseName("Mathematics")
                    .students(new ArrayList<>())
                    .build();
  }

  @Test
  void testGivenCourseDTO_WhenCreateCourse_ShouldReturnCourse() {
    given(coursesRepository.save(any(Courses.class))).willAnswer(invocation -> invocation.getArgument(0));
    given(coursesRepository.findCourseByCourseName(course.getCourseName())).willReturn(Optional.empty());

    Courses savedCourse = createCourseUseCaseImpl.execute(course);

    assertNotNull(savedCourse);
    assertEquals(course.getCourseName(), savedCourse.getCourseName());
  }

  @Test
  void testGivenCourseDTOWithExistingCourseName_WhenCreateCourse_ShouldThrowException() {
    given(coursesRepository.findCourseByCourseName(course.getCourseName())).willReturn(Optional.of(course));

    assertThrows(InvalidRequestException.class, () -> {
      createCourseUseCaseImpl.execute(course);
    });

    verify(coursesRepository, never()).save(any(Courses.class));
  }
}

