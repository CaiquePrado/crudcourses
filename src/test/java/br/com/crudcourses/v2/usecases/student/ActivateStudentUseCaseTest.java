package br.com.crudcourses.v2.usecases.student;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.impl.ActivateStudentUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class ActivateStudentUseCaseTest {

  @Mock
  FindStudentByCpfUseCase findStudentByCpfUseCase;

  @Mock
  StudentRepository studentRepository;

  @InjectMocks
  ActivateStudentUseCaseImpl activateStudentUseCaseImpl;

  Student existingStudent;
  String cpf;

  @BeforeEach
  void setup() {
    cpf = "10512971601";
    existingStudent = createStudent(UUID.randomUUID(), "Roger", 30, cpf, true);
  }

  @Test
  void testGivenValidCpfAndInactiveStudent_WhenActivateStudent_ShouldSetActiveToTrue() {
    Student inactiveStudent = createStudent(UUID.randomUUID(), "Roger", 30, cpf, false);
    given(findStudentByCpfUseCase.find(cpf)).willReturn(inactiveStudent);

    Student activatedStudent = activateStudentUseCaseImpl.execute(cpf);

    assertTrue(activatedStudent.getActive());
    verify(studentRepository).save(activatedStudent);
  }

  @Test
  void testGivenInvalidCpf_WhenActivateStudent_ShouldThrowException() {
    String invalidCpf = "12345678901";
    given(findStudentByCpfUseCase.find(invalidCpf)).willThrow(new IllegalArgumentException("Student not found"));

    assertThrows(IllegalArgumentException.class, () -> {
      activateStudentUseCaseImpl.execute(invalidCpf);
    });

    verifyNoInteractions(studentRepository);
  }

  @Test
  void testGivenAlreadyActivatedStudent_WhenActivateStudent_ShouldThrowException() {
    given(findStudentByCpfUseCase.find(cpf)).willReturn(existingStudent);

    assertThrows(InvalidRequestException.class, () -> {
      activateStudentUseCaseImpl.execute(cpf);
    });

    verifyNoInteractions(studentRepository);
  }

  private Student createStudent(UUID id, String name, int age, String cpf, boolean active) {
    return Student.builder()
                  .id(id)
                  .name(name)
                  .age(age)
                  .cpf(cpf)
                  .active(active)
                  .build();
  }
}

