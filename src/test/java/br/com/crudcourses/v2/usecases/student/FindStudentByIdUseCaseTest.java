package br.com.crudcourses.v2.usecases.student;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.ResourceNotFoundException;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.student.impl.FindStudentByIdUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class FindStudentByIdUseCaseTest {
  
@Mock
  StudentRepository studentRepository;

  @InjectMocks
  FindStudentByIdUseCaseImpl findStudentByIdUseCaseImpl;

  Student student;
  UUID studentId;

  @BeforeEach
  void setup() {
    studentId = UUID.randomUUID();
    student = Student.builder()
                    .id(studentId)
                    .name("John Doe")
                    .age(25)
                    .build();
  }


  @Test
  void testGivenValidCourseId_WhenFindCourseById_ShouldReturnCourse() {
    
    given(studentRepository.findById(studentId)).willReturn(Optional.of(student));

    Student foundStudent = findStudentByIdUseCaseImpl.find(studentId);

    assertNotNull(foundStudent);
    assertEquals(studentId, foundStudent.getId());
    assertEquals(student.getName(), foundStudent.getName());
    assertEquals(student.getAge(), foundStudent.getAge());

  }

  @Test
  void testGivenInvalidCourseId_WhenFindCourseById_ShouldThrowResourceNotFoundException() {
    
    given(studentRepository.findById(studentId)).willReturn(Optional.empty());

    assertThrows(ResourceNotFoundException.class, () -> {
      findStudentByIdUseCaseImpl.find(studentId);
    });
  }
}

