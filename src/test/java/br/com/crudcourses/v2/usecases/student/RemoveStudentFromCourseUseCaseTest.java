package br.com.crudcourses.v2.usecases.student;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.ResourceNotFoundException;
import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.models.Student;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.repository.StudentRepository;
import br.com.crudcourses.v2.usecases.courses.FindCourseByIdUseCase;
import br.com.crudcourses.v2.usecases.student.impl.RemoveStudentFromCourseUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class RemoveStudentFromCourseUseCaseTest {

  @Mock
  FindStudentByIdUseCase findStudentByIdUseCase;

  @Mock
  FindCourseByIdUseCase findCourseByIdUseCase;

  @Mock
  CoursesRepository courseRepository;

  @Mock
  StudentRepository studentRepository;

  @InjectMocks
  RemoveStudentFromCourseUseCaseImpl removeStudentFromCourseUseCase;

  UUID courseId;
  UUID studentId;
  Courses course;
  Student student;

  @BeforeEach
  void setup() {
    courseId = UUID.randomUUID();
    studentId = UUID.randomUUID();

    student = Student.builder()
                     .id(studentId)
                     .build();

    course = Courses.builder()
                    .id(courseId)
                    .courseName("Mathematics")
                    .students(new ArrayList<>(Arrays.asList(student)))
                    .createdAt(LocalDateTime.now())
                    .build();
  }

  @DisplayName("Given Course and Student IDs when Remove Student from Course should Succeed")
  @Test
  void testGivenCourseAndStudentIDs_WhenRemoveStudentFromCourse_ShouldSucceed() {
    given(findCourseByIdUseCase.find(courseId)).willReturn(course);
    given(findStudentByIdUseCase.find(studentId)).willReturn(student);

    removeStudentFromCourseUseCase.execute(courseId, studentId);

    assertFalse(course.getStudents().contains(student));
    verify(courseRepository, times(1)).save(course);
  }

  @DisplayName("Given invalid Course ID when Remove Student from Course should Throw Exception")
  @Test
  void testGivenInvalidCourseID_WhenRemoveStudentFromCourse_ShouldThrowException() {
    given(findCourseByIdUseCase.find(courseId)).willThrow(new ResourceNotFoundException("Course not found with id: " + courseId));

    assertThrows(ResourceNotFoundException.class, () -> {
      removeStudentFromCourseUseCase.execute(courseId, studentId);
    });
  }

  @DisplayName("Given invalid Student ID when Remove Student from Course should Throw Exception")
  @Test
  void testGivenInvalidStudentID_WhenRemoveStudentFromCourse_ShouldThrowException() {
    given(findCourseByIdUseCase.find(courseId)).willReturn(course);
    given(findStudentByIdUseCase.find(studentId)).willThrow(new ResourceNotFoundException("Student not found with id: " + courseId));

    assertThrows(ResourceNotFoundException.class, () -> {
      removeStudentFromCourseUseCase.execute(courseId, studentId);
    });
  }
}
