package br.com.crudcourses.v2.usecases.teacher;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.impl.FindTeacherByIdUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class FindTeacherByIdUseCaseTest {
  
  @Mock
  TeacherRepository teacherRepository;

  @InjectMocks
  FindTeacherByIdUseCaseImpl findTeacherByIdUseCaseImpl;

  Teacher teacher;
  UUID teacherId;

  @BeforeEach
  void setup() {
    teacherId = UUID.randomUUID();
    teacher = Teacher.builder()
                    .id(teacherId)
                    .name("Roger")
                    .age(20)
                    .cpf("10512971601")
                    .workCard("WC123")
                    .salary(5000.00)
                    .build();
  }


  @Test
  void testGivenValidTeacherId_WhenFindTeacherById_ShouldReturnTeacher() {

    given(teacherRepository.findById(teacherId)).willReturn(Optional.of(teacher));

    Teacher foundTeacher = findTeacherByIdUseCaseImpl.find(teacherId);

    assertNotNull(foundTeacher);
    assertEquals(teacherId, foundTeacher.getId());
    assertEquals(teacher.getName(), foundTeacher.getName());
    assertEquals(teacher.getAge(), foundTeacher.getAge());
  }

  @Test
  void testGivenInvalidTeacherId_WhenFindTeacherById_ShouldThrowIllegalArgumentException() {

    given(teacherRepository.findById(teacherId)).willReturn(Optional.empty());

    assertThrows(IllegalArgumentException.class, () -> {
      findTeacherByIdUseCaseImpl.find(teacherId);
    });
  }
}

