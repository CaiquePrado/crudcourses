package br.com.crudcourses.v2.usecases.courses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Courses;
import br.com.crudcourses.v2.repository.CoursesRepository;
import br.com.crudcourses.v2.usecases.courses.impl.FindCourseByIdUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class FindCourseByIdUseCaseTest {
  
  @Mock
  CoursesRepository coursesRepository;

  @InjectMocks
  FindCourseByIdUseCaseImpl findCourseByIdUseCaseImpl;

  Courses course;
  UUID courseId;

  @BeforeEach
  void setup() {
    courseId = UUID.randomUUID();
    course = Courses.builder()
                    .id(courseId)
                    .courseName("Mathematics")
                    .students(new ArrayList<>())
                    .build();
  }

  @Test
  void testGivenValidCourseId_WhenFindCourseById_ShouldReturnCourse() {
    
    given(coursesRepository.findById(courseId)).willReturn(Optional.of(course));

    Courses foundCourse = findCourseByIdUseCaseImpl.find(courseId);

    assertNotNull(foundCourse);
    assertEquals(courseId, foundCourse.getId());
    assertEquals(course.getCourseName(), foundCourse.getCourseName());
  }

  @Test
  void testGivenInvalidCourseId_WhenFindCourseById_ShouldThrowResourceNotFoundException() {
    
    given(coursesRepository.findById(courseId)).willReturn(Optional.empty());

    assertThrows(InvalidRequestException.class, () -> {
      findCourseByIdUseCaseImpl.find(courseId);
    });
  }
}

