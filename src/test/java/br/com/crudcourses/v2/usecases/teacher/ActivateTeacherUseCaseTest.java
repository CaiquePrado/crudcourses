package br.com.crudcourses.v2.usecases.teacher;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.impl.ActivateTeacherUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class ActivateTeacherUseCaseTest {

  @Mock
  TeacherRepository teacherRepository;

  @Mock
  FindTeacherByCpfUseCase findTeacherByCpfUseCase;

  @InjectMocks
  ActivateTeacherUseCaseImpl activateTeacherUseCaseImpl;
  
  Teacher existingTeacher;
  UUID id;

  @BeforeEach
  void setup() {
    id = UUID.randomUUID();
    existingTeacher = Teacher.builder()
                             .id(id)
                             .name("Roger")
                             .age(30)
                             .cpf("10512971601")
                             .workCard("WC123")
                             .salary(5000.00)
                             .active(true)
                             .build();
  }

  @Test
  void testGivenValidIdAndInactiveTeacher_WhenActivateTeacher_ShouldSetActiveToTrue() {

  Teacher inactiveTeacher = Teacher.builder()
                                   .id(id)
                                   .name("Roger")
                                   .age(30)
                                   .cpf("10512971601")
                                   .workCard("WC123")
                                   .salary(5000.00)
                                   .active(false)
                                   .build();
    given(findTeacherByCpfUseCase.find(existingTeacher.getCpf())).willReturn(inactiveTeacher);

    Teacher activatedTeacher = activateTeacherUseCaseImpl.execute(existingTeacher.getCpf());

    assertTrue(activatedTeacher.getActive());
    verify(teacherRepository).save(activatedTeacher);
}

  @Test
  void testGivenInvalidId_WhenActivateTeacher_ShouldThrowException() {

    String invalidCpf = "84uh3uibsdfg7sf7sf";
    given(findTeacherByCpfUseCase.find(invalidCpf)).willThrow(new IllegalArgumentException());

    assertThrows(IllegalArgumentException.class, () -> {
      activateTeacherUseCaseImpl.execute(invalidCpf);
    });

    verify(findTeacherByCpfUseCase).find(invalidCpf);
    verifyNoInteractions(teacherRepository);
}

  @Test
  void testGivenAlreadyActivatedTeacher_WhenActivateTeacher_ShouldThrowException() {
    
    given(findTeacherByCpfUseCase.find(existingTeacher.getCpf())).willReturn(existingTeacher);

    assertThrows(InvalidRequestException.class, () -> {
      activateTeacherUseCaseImpl.execute(existingTeacher.getCpf());
    });

    verify(findTeacherByCpfUseCase).find(existingTeacher.getCpf());
    verifyNoInteractions(teacherRepository);
  }
}
