package br.com.crudcourses.v2.usecases.teacher;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.crudcourses.v2.infra.exceptions.InvalidRequestException;
import br.com.crudcourses.v2.models.Teacher;
import br.com.crudcourses.v2.repository.TeacherRepository;
import br.com.crudcourses.v2.usecases.teacher.impl.CreateTeacherUseCaseImpl;

@ExtendWith(MockitoExtension.class)
class CreateTeacherUseCaseTest {
  
  @Mock
  TeacherRepository teacherRepository;

  @InjectMocks
  CreateTeacherUseCaseImpl createTeacherUseCaseImpl;

  Teacher teacher;

  @BeforeEach
  void setup(){
    teacher = Teacher.builder()
                     .name("Roger")
                     .age(20)
                     .cpf("10512971601")
                     .workCard("WC123")
                     .salary(5000.00)
                     .build();
  }

  @Test
  void testGivenTeacherObject_WhenSaveTeacher_ShouldReturnTeacher() {

    given(teacherRepository.save(any(Teacher.class))).willAnswer(invocation -> invocation.getArgument(0));

    Teacher savedTeacher = createTeacherUseCaseImpl.execute(teacher);

    assertNotNull(savedTeacher);
    assertSame(teacher, savedTeacher);
    verify(teacherRepository).save(teacher);
  }

  @Test
  void testGivenTeacherObjectWithExistingCPF_WhenSaveTeacher_ShouldThrowDataIntegrityViolationException() {

    given(teacherRepository.findByCpf(anyString())).willReturn(Optional.of(teacher));

    assertThrows(InvalidRequestException.class, () -> {
      createTeacherUseCaseImpl.execute(teacher);
    });

    verify(teacherRepository, never()).save(any(Teacher.class));
  }

  @Test
  void testGivenTeacherObjectWithExistingWorkCard_WhenSaveTeacher_ShouldThrowDataIntegrityViolationException() {

    given(teacherRepository.findByWorkCard(anyString())).willReturn(Optional.of(teacher));

    assertThrows(InvalidRequestException.class, () -> {
      createTeacherUseCaseImpl.execute(teacher);
    });

    verify(teacherRepository, never()).save(any(Teacher.class));
  }
}

