package br.com.crudcourses.v2.utils;

import java.util.UUID;

import br.com.crudcourses.v2.request.RequestCreateCourseDTO;
import br.com.crudcourses.v2.request.UpdateCourseDTO;

public class CoursesEntitiesBuilder {

  public static RequestCreateCourseDTO validCourseDTOBuilder() {
      return new RequestCreateCourseDTO("Mathematics", UUID.fromString("7a7b8ccf-9ba1-4ec7-b378-f69e84238b24"));
  }

  public static RequestCreateCourseDTO invalidCourseDTOBuilder() {
      return new RequestCreateCourseDTO("", UUID.randomUUID());
  }

  public static RequestCreateCourseDTO duplicateCourseNameDTOBuilder(String existingCourseName) {
      return new RequestCreateCourseDTO(existingCourseName, UUID.randomUUID());
  }

  public static UpdateCourseDTO updateCourseDTOBuilder() {
        return new UpdateCourseDTO("Updated Course Name",UUID.fromString("590b06f9-79fb-49d0-bb2b-0b0b2e1838d9"));
  }
}
