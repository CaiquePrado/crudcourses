package br.com.crudcourses.v2.utils;

public class SqlProvider {
  public static final String clearDB= "/clear.sql";
  public static final String insertStutends= "/insertStutends.sql";
  public static final String insertTeachers= "/insertTeachers.sql";
  public static final String insertCourses= "/insertCourses.sql";
}
