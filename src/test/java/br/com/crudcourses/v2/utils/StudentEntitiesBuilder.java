package br.com.crudcourses.v2.utils;

import br.com.crudcourses.v2.request.RequestCreateStudentDTO;
import br.com.crudcourses.v2.request.UpdateStudentOrTeacherDTO;

public class StudentEntitiesBuilder {

  public static RequestCreateStudentDTO validStudentDTOBuilder() {
      return new RequestCreateStudentDTO("João", 20, "10512971501");
  }

  public static RequestCreateStudentDTO invalidStudentDTOBuilder() {
      return new RequestCreateStudentDTO("", 20, "");
  }

  public static RequestCreateStudentDTO duplicateCpfStudentDTOBuilder(String existingCpf) {
      return new RequestCreateStudentDTO("João", 22, existingCpf);
  }

  public static UpdateStudentOrTeacherDTO updateStudentDTOBuilder() {
        return new UpdateStudentOrTeacherDTO("Updated Name", 22);
  }
}
