package br.com.crudcourses.v2.utils;

import br.com.crudcourses.v2.request.RequestCreateTeacherDTO;
import br.com.crudcourses.v2.request.UpdateStudentOrTeacherDTO;

public class TeacherEntitiesBuilder {

  public static RequestCreateTeacherDTO validTeacherDTOBuilder() {
      return new RequestCreateTeacherDTO("João", 20, "10512971501", "10512971501", 5000.0);
  }

  public static RequestCreateTeacherDTO invalidTeacherDTOBuilder() {
      return new RequestCreateTeacherDTO("", 20, "", "", 0.0);
  }

  public static RequestCreateTeacherDTO duplicateCpfTeacherDTOBuilder(String existingCpf) {
      return new RequestCreateTeacherDTO("João", 22, existingCpf, "10512971501", 5500.0);
  }

  public static UpdateStudentOrTeacherDTO updateTeacherDTOBuilder() {
        return new UpdateStudentOrTeacherDTO("Updated Name", 22);
  }
}
