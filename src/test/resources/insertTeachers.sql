INSERT INTO tb_teacher (active, age, salary, created_at, cpf, id, name, work_card)
VALUES (true, 30, 6000, '2024-04-01 14:52:20.123456', '12345678901', '7a7b8ccf-9ba1-4ec7-b378-f69e84238b24', 'Alice', '12345678901');

INSERT INTO tb_teacher (active, age, salary, created_at, cpf, id, name, work_card)
VALUES (false, 25, 5500, '2024-04-01 14:53:30.654321', '98765432109', '1b2c3d4e-5f6a-7b8c-d9e0-f1a2b3c4d5e6', 'Bob', '98765432109');

INSERT INTO tb_teacher (active, age, salary, created_at, cpf, id, name, work_card)
VALUES (true, 35, 7000, '2024-04-01 14:54:40.987654', '24680135792', 'f1a2b3c4-d5e6-f7a8-b9c0-d2e3f4a5b6c7', 'Eve', '24680135792');