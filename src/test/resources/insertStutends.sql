INSERT INTO tb_student (active, age, created_at, cpf, id, name)
VALUES (true, 40, '2024-04-01 14:51:42.665082', '59364813014', '1dc5e468-ff87-4afb-a071-a9861f7ac274', 'Pedro');

INSERT INTO tb_student (active, age, created_at, cpf, id, name)
VALUES (false, 32, '2024-04-01 14:51:49.91596', '51749469073', 'c246d60f-1665-4c94-82f9-f41a7cd26559', 'Huelton');

INSERT INTO tb_student (active, age, created_at, cpf, id, name)
VALUES (false, 22, '2024-04-01 14:51:54.564125', '12288419008', '7b62d418-395a-468f-a67b-0c2a7a0ec495', 'Gabriel');

INSERT INTO tb_student (active, age, created_at, cpf, id, name)
VALUES (true, 18, '2024-04-01 14:51:58.907543', '91368318088', '01ef7fff-26fa-4ba8-bc9f-5ed97f0bab8b', 'Dovani');

